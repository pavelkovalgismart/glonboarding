# GLOnboarding

[![CI Status](https://img.shields.io/travis/Pavel Koval/GLOnboarding.svg?style=flat)](https://travis-ci.org/Pavel Koval/GLOnboarding)
[![Version](https://img.shields.io/cocoapods/v/GLOnboarding.svg?style=flat)](https://cocoapods.org/pods/GLOnboarding)
[![License](https://img.shields.io/cocoapods/l/GLOnboarding.svg?style=flat)](https://cocoapods.org/pods/GLOnboarding)
[![Platform](https://img.shields.io/cocoapods/p/GLOnboarding.svg?style=flat)](https://cocoapods.org/pods/GLOnboarding)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GLOnboarding is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GLOnboarding'
```

## Author

Pavel Koval, pavel.koval@gismart.com

## License

GLOnboarding is available under the MIT license. See the LICENSE file for more info.
