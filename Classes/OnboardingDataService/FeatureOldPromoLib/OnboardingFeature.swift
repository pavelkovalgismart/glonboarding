//
//  OnboardingDataService.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class OnboardingFeature: GLRCFeature {
    enum Key {
        static let featureName = "onboarding_feature"
        static let inAppIdValue = "inapp_id"
        static let subscriptionPriceValue = "subscription_price"
        static let subscriptionButtonTextValue = "subscription_button_text"
        static let pages = "pages"
        static let askAgainNotification = "ask_again_notification"
        static let firstHtmlNotification = "first_html_cancel_popup_model"
        static let secondHtmlNotification = "second_html_cancel_popup_model"
        static let firstHtmlPopupShowIndexes = "first_html_cancel_popup_indexes"
        static let seconHtmlPopupShowIndexes = "second_html_cancel_popup_indexes"
        static let idleTrialPromotionTimeout = "idle_trial_promotion_timeout"
        static let idleTrialPromotionNotification = "idle_trial_promotion_notification"
        static let continueFreePopupEnabled = "continue_free_popup_enabled"
    }
    
    override class func featureName() -> String {
        return Key.featureName
    }
}

extension OnboardingFeature: OnboardingSettings {
    
    var continueFreePopupEnabled: Bool? {
        return self.values[Key.continueFreePopupEnabled] as? Bool
    }

    var pages: [[String : Any]]? {
        return self.values[Key.pages] as? [[String : Any]]
    }

    var inAppId: String? {
        return self.values[Key.inAppIdValue] as? String
    }

    var subscriptionPrice:String? {
        return self.values[Key.subscriptionPriceValue] as? String
    }

    var firstHtmlPopupShowIndexes: [Int]? {
        return self.values[Key.firstHtmlPopupShowIndexes] as? [Int]
    }

    var seconHtmlPopupShowIndexes: [Int]? {
        return self.values[Key.seconHtmlPopupShowIndexes] as? [Int]
    }

    var firstHtmlPopupData: [String : Any]? {
        return self.values[Key.firstHtmlNotification] as? [String : Any]
    }

    var secondHtmlPopupData: [String : Any]? {
        return self.values?[Key.secondHtmlNotification] as? [String : Any]
    }

    var askAgainPopupData: [String : Any]? {
        return self.values[Key.askAgainNotification] as? [String : Any]
    }
}

