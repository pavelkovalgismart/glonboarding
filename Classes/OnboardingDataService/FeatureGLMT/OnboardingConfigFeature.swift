//
//  OnboardingConfigFeature.swift
//  RealTuner
//
//  Created by Nikolay Tsygankov on 11/12/2018.
//  Copyright © 2018 GiSmart. All rights reserved.
//

import Foundation
import glmt

enum OnboardingVersion: Int {
    case new = 0
    case old = 1
}

struct OnboardingConfigFeature: AppFeature, OnboardingSettings {
    let pages: [[String : Any]]?
    let inAppId: String?
    let continueFreePopupEnabled: Bool?
    let firstHtmlPopupShowIndexes: [Int]?
    let seconHtmlPopupShowIndexes: [Int]?
    let firstHtmlPopupData: [String : Any]?
    let secondHtmlPopupData: [String : Any]?
    let askAgainPopupData: [String : Any]?
    let richCoef: Double?
    let showCloseButton: Bool
    let onboardingVersion: OnboardingVersion

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if var pagesRaw = try? container.nestedUnkeyedContainer(forKey: .pages) {
            self.pages = try? pagesRaw.decodeValidElements([String : Any].self)
        } else {
            self.pages = nil
        }
        self.inAppId = try? container.decode(String.self, forKey: .inAppId)
        self.askAgainPopupData = try? container.decode([String : Any].self, forKey: .askAgainPopupData)
        self.firstHtmlPopupShowIndexes = try? container.decode([Int].self, forKey: .firstHtmlPopupShowIndexes)
        self.firstHtmlPopupData = try? container.decode([String : Any].self, forKey: .firstHtmlPopupData)
        self.seconHtmlPopupShowIndexes = try? container.decode([Int].self, forKey: .seconHtmlPopupShowIndexes)
        self.secondHtmlPopupData = try? container.decode([String : Any].self, forKey: .secondHtmlPopupData)
        self.richCoef = try? container.decode(Double.self, forKey: .richCoef)
        self.showCloseButton = (try? container.decode(Bool.self, forKey: .showCloseButton)) ?? false
        self.continueFreePopupEnabled = try? container.decode(Bool.self, forKey: .continueFreePopupEnabled)
        let version = (try? container.decode(Int.self, forKey: .onboardingVersion)) ?? 0
        self.onboardingVersion = OnboardingVersion(rawValue: version) ?? .old
    }

    private enum CodingKeys: String, CodingKey {
        case inAppId = "inapp_id"
        case subscriptionButtonTextValue = "subscription_button_text"
        case pages = "pages"
        case askAgainPopupData = "ask_again_notification"
        case firstHtmlPopupData = "first_html_cancel_popup_model"
        case secondHtmlPopupData = "second_html_cancel_popup_model"
        case firstHtmlPopupShowIndexes = "first_html_cancel_popup_indexes"
        case seconHtmlPopupShowIndexes = "second_html_cancel_popup_indexes"
        case richCoef = "richCoef"
        case showCloseButton = "show_close_button"
        case idlePopupTimeOut = "idle_trial_promotion_timeout"
        case idlePopupData = "idle_trial_promotion_notification"
        case continueFreePopupEnabled = "continue_free_popup_enabled"
        case onboardingVersion = "onboarding_version"
    }
}
