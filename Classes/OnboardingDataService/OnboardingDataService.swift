//
//  OnboardingDataService.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

protocol OnboardingSettings {
    var pages: [[String : Any]]? {get}
    var inAppId: String? {get}
}

class OnboardingDataService {
    
    var settings: OnboardingSettings
    
    init(settings: OnboardingSettings) {
        self.settings = settings
    }
    
    func getOnboardingData() -> [PageModel] {
        var pages = [PageModel]()
        settings.pages?.forEach({ (pageDict) in
            if let pageModel = PageModelFactory.makePageModel(with: pageDict) {
                pages.append(pageModel)
            }
        })
        return pages
    }
}
