//
//  OnboardingPresenter.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation
import StoreKit
import glprobabilitypurchaseanalyzer
import glanalytics

@objc protocol OnboardingPresenterDelegate: class {
    func onboardingWillClose()
    func onboardingDidClose()
}

class OnboardingPresenter {

    weak var delegate: OnboardingPresenterDelegate?
    private weak var view: (OnboardingView & UIViewController)?
    fileprivate let dataService: OnboardingDataService
    fileprivate var pagesData: [PageModel] = []
    fileprivate var logger: AnalyticEventsLogging?
    let serialQueId = "drums.onboarding"
    var inappId: String?
    var subscriptionPrice: String?
    let inAppPurchaseManager: OnboardingPurchaseManager

    init(dataService: OnboardingDataService, logger: AnalyticEventsLogging?, inAppPurchaseManager: OnboardingPurchaseManager, view: (OnboardingView & UIViewController)) {
        self.dataService = dataService
        self.view = view
        self.pagesData = self.dataService.getOnboardingData()
        self.inappId = self.dataService.settings.inAppId
        self.logger = logger
        self.inAppPurchaseManager = inAppPurchaseManager
        self.subscribeOnNotifications()
        self.updatePrice()
    }

    func canBeShowed() -> Bool {
        guard self.pagesData.count > 0 else { return false }
        guard self.inAppPurchaseManager.isBundlePurchased() == false else { return false }
        return true
    }

    private func dismissView() {
        self.delegate?.onboardingWillClose()
        self.view?.close {
            self.delegate?.onboardingDidClose()
        }
    }

    //MARK: - Private
    private func updatePrice() {
        guard let inappId = self.inappId else {
            return
        }
        self.inAppPurchaseManager.product(with: inappId) { [weak self] (product, error) in
            if let product = product, let price = product.localizedPrice {
                self?.subscriptionPrice = price
                self?.view?.updatePurchasePageData()
            }
        }
    }
    
    private func subscribeOnNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(OnboardingPresenter.updateInappsInfo), name: inAppPurchaseManager.purchaseItemsUpdateKey, object: nil)
        if let promotedIApClickedName = self.inAppPurchaseManager.promotedIapClicked {
            NotificationCenter.default.addObserver(self, selector: #selector(OnboardingPresenter.promotedIApClicked), name: promotedIApClickedName, object: nil)
        }
    }

    @objc private func updateInappsInfo() {
        DispatchQueue.main.async {
            self.updatePrice()
        }
    }
    
    @objc private func promotedIApClicked() {
        DispatchQueue.main.async { [weak self] in
            self?.view?.goToShopPage()
        }
    }
}

//MARK: - OnboardingViewDataSource
extension OnboardingPresenter: OnboardingViewDataSource {
    func inappPrice() -> String? {
        return self.subscriptionPrice
    }

    func onboardingPage(at index: Int) -> PageModel? {
        guard index < self.pagesData.count else { return nil }
        return self.pagesData[index]
    }

    func countOfPages() -> Int {
        return self.pagesData.count
    }
    
    func nextButtonText(forPageAt index: Int) -> String? {
        let model = self.onboardingPage(at: index)
        if model is PurchasePageModel, self.subscriptionPrice == nil {
            return Localizations.Loading
        } else {
            return model?.buttonText
        }
    }
}

//MARK: OnboardingViewDelegate
extension OnboardingPresenter: OnboardingViewDelegate {
    func viewDidLoad() {
        self.updatePrice()
        DispatchQueue(label: serialQueId).async {
            self.logger?.log(event: OnboardingAppearedAnalyticEvent())
        }
    }

    func currentPageChanged(to pageIndex: Int, fromPageIndex: Int, slide: Bool) {
        guard abs(pageIndex - fromPageIndex) == 1 else { return }
        let isLastPage = self.countOfPages() - 1 == pageIndex
        if isLastPage {
            updatePrice()
        }
        DispatchQueue(label: serialQueId).async {
            self.logger?.log(event: OnboardingStepAppearedAnalyticEvent(stepIndex: pageIndex, previousStepIndex: fromPageIndex))
        }
    }

    func closeAction() {
        self.logger?.log(event: OnboardingPassedFreeAnalyticEvent())
        self.logger?.log(event: CommonAnalyticEvent(name: CustomEvents.onboardingClosePressed, parameters: [:]))
        self.dismissView()
        
    }

    func purchaseButtonDidTap() {
        self.logger?.log(event: CommonAnalyticEvent(name: CustomEvents.onboardingStartTrialPressed, parameters: [:]))
        guard let inappId = self.inappId else { return }

        view?.showActivity(show: true)
        self.updatePrice()
        self.inAppPurchaseManager.onboarding_addPayment(inappId, additionalParams: ["purchase_source" : "on_boarding"], success: {
            self.updatePrice()
            self.view?.nextButton.stopSound()
            self.view?.showActivity(show: false)
            self.dismissView()
            self.logger?.log(event:OnboardingPassedAnalyticEvent())
        }) { (error) in
            self.view?.nextButton.stopSound()
            self.view?.showActivity(show: false)
            self.updatePrice()
        }
    }

    func restoreAction() {
        guard let view = self.view else { return }
        view.showActivity(show: true)
        self.inAppPurchaseManager.onboarding_restore(success: {
            view.showActivity(show: false)
            if self.inAppPurchaseManager.isBundlePurchased() {
                self.dismissView()
                self.logger?.log(event: OnboardingPassedByRestoreAnalyticEvent())
            }
        }) { error in
            view.showActivity(show: false)
        }
        return
    }

    func continueFreeButtonDidTap() {
    }
}
