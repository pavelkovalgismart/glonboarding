//
//  OnboardingView.swift
//  RealTuner
//
//  Created by Nikolay Tsygankov on 14/11/2018.
//  Copyright © 2018 GiSmart. All rights reserved.
//

import Foundation

protocol OnboardingViewDelegate: class {
    func viewDidLoad()
    func purchaseButtonDidTap()
    func continueFreeButtonDidTap()
    func currentPageChanged(to pageIndex: Int, fromPageIndex: Int, slide: Bool)
    func closeAction()
    func restoreAction()
}

protocol OnboardingViewDataSource: class {
    func onboardingPage(at index: Int) -> PageModel?
    func countOfPages() -> Int
    func inappPrice() -> String?
    func nextButtonText(forPageAt index: Int) -> String?
}

protocol OnboardingView: class {
    var delegate: OnboardingViewDelegate? {get set}
    var presenter: OnboardingPresenter? {get set}
    var dataSource: OnboardingViewDataSource? {get set}
    var nextButton: (OnboardingButton & UIButton)! {get set}
    func showActivity(show: Bool)
    func updatePurchasePageData()
    func goToShopPage()
    func close(completion: (() -> Void)?)
}


@objc extension UIViewController {
    @objc func isVcOnboarding() -> Bool {
        return self is OnboardingView
    }
}
