//
//  OnboardingButtomProtocol.swift
//  RealGuitar
//
//  Created by Alex on 5/23/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

//TODO: - Remove this protocol
protocol OnboardingButton: class {
    func startAnimation()
    func didTouchUpInside(from page: Int)
    func setTitle(_ title: String?, priceDescr: String?, showArrow: Bool, animated: Bool)
    func setTitle(_ title: String?, lastPage: Bool)
    func setupForView(_ view: UIView)
    func stopSound()
}
