//
//  PassThroughScrollView.swift
//  RealDrumFree
//
//  Created by NIkolay Tsygankov on 3/14/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

struct TouchableView {
    let view: UIView
    let superView: UIView
    let isOnScreen: () -> Bool
}

class PassThroughScrollView: MOScrollView {
    var touchableViews: [TouchableView] = []
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        var isNeedIgnoreEvent = false
        for view in self.touchableViews {
            let rect = self.convert(view.view.frame, from: view.superView)
            if rect.contains(point) && view.isOnScreen() {
                isNeedIgnoreEvent = true
                break
            }
        }
        return !isNeedIgnoreEvent
    }
}
