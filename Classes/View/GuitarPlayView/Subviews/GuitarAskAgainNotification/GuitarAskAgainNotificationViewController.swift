//
//  GuitarAskAgainNotificationViewController.swift
//  RealGuitarLite
//
//  Created by Alex Parkhimovich on 6/20/18.
//  Copyright © 2018 Alex Parhimovich. All rights reserved.
//

import UIKit

class GuitarAskAgainNotificationViewController: AskAgainNotificationViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelImage = UIImage(named: "guitar_ask_again_cancel_button")
        let okImage = UIImage(named: "guitar_ask_again_ok_button")
        let iconImage = UIImage(named: "guitar_ask_again_icon")

        self.cancelButton.setBackgroundImage(cancelImage, for: .normal)
        self.okButton.setBackgroundImage(okImage, for: .normal)
        self.iconImageView.image = iconImage
    }
    
    override class func create() -> GuitarAskAgainNotificationViewController {
        let popup = GuitarAskAgainNotificationViewController(nibName: "AskAgainNotificationViewController", bundle: Bundle.main)
        popup.modalTransitionStyle = .crossDissolve
        popup.modalPresentationStyle = .overCurrentContext
        return popup
    }
}
