//
//  PurchaseTextPage.swift
//  RealDrumFree
//
//  Created by Nikolay Tsygankov on 06/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

protocol PurchaseTextPageDelegate: class {
    func continueFreeButtonDidTap()
    func closeAction()
}

class PurchaseTextPage: CustomXibView, PageSide {
    // Default from xib
    private static let defaultTitleTopPadding: CGFloat = 20
    
    var animationView: UIView {
        return self
    }

    var animator: OnboardingPageAnimator?
    
    weak var delegate: PurchaseTextPageDelegate?
    
    private(set) var touchableSubviews: [TouchableView] = []
    @IBOutlet var bodyLabels: [UILabel]!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleStackView: UIStackView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var secondSubtitleLabel: UILabel!

    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bodyStackView: UIStackView!
    @IBOutlet weak var continueFreeLabel: UILabel!
    @IBOutlet weak var hiddenLabel: UILabel!
    
    @IBOutlet weak var disclamerVerticalOffsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewLeadingViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewLeadingSafeConstraint: NSLayoutConstraint!
    
    @IBAction func onCloseClicked(_ sender: Any) {
        self.delegate?.closeAction()
    }
    
    override func customize() {
        super.customize()
        NotificationCenter.default.addObserver(self, selector: #selector(self.deviceRotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        if !OnboardingUtils.isIphone() {
            self.titleLabel.numberOfLines = 0
            self.titleTopConstraint.constant = 138
            self.stackView.spacing = 26
            self.titleLabel.font = UIFont.systemFont(ofSize: 40, weight: .heavy)
            self.subtitleLabel.font = UIFont.systemFont(ofSize: 26, weight: .heavy)
        } else if OnboardingUtils.DeviceType.IS_IPHONE_5 {
            self.titleTopConstraint.constant = 24
            self.stackView.spacing = 8
            self.titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .heavy)
            self.subtitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .heavy)
        } else if OnboardingUtils.DeviceType.IS_IPHONE_6 {
            self.titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .heavy)
            self.subtitleLabel.font = UIFont.systemFont(ofSize: 13, weight: .heavy)
        }
        self.hiddenLabel.font = self.titleLabel.font

        self.touchableSubviews.append(TouchableView(view: self.scrollView, superView: self, isOnScreen: {
            return self.alpha != 0
        }))
    }
    
    func setup(title: String?, subtitle: String?, bodyStrings: [String], subscriptionPrice: String?, policyUrl: String?, termUrlString: String?, termsInfo: String?, buttonTitle: String?, inappDescription: String?, crossType: OnboardingCrossType?) {
        
        self.titleLabel.text = title

        self.subtitleLabel.text = subtitle

        if let inappDescription = inappDescription, let subscriptionPrice = subscriptionPrice {
            self.secondSubtitleLabel.text = String(format: inappDescription, subscriptionPrice)
        }
        
        for (index, lbl) in self.bodyLabels.enumerated() {
            guard index < bodyStrings.count else { continue }
            
            let text: NSMutableAttributedString = NSMutableAttributedString(string:bodyStrings[index])
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .left
            paragraphStyle.baseWritingDirection = .rightToLeft
            paragraphStyle.lineBreakMode = .byWordWrapping
            
            text.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, text.length))
            if isIpad {
                lbl.font = lbl.font.withSize(23)
            } else {
                lbl.font = lbl.font.withSize(iphone5AndLess ? 12 : 15)
            }
            
            lbl.attributedText = text

        }
        if let policy = policyUrl, let terms = termUrlString, let termsInfo = termsInfo {
            
            let (fontName, fontSize, _) = self.infoTextAttributes
            
            let infoText = NSAttributedString.purchaseInfoText(from: termsInfo, policyUrl: policy, termsUrl: terms, fontName: fontName, fontSize: Float(fontSize), align: NSTextAlignment.justified)
            self.textView.attributedText = infoText
        }
        self.textView.scrollRangeToVisible(NSMakeRange(0, 0))

        //continue lbl

        if isIpad {
            self.continueFreeLabel.font = self.continueFreeLabel.font.withSize(23)
        } else {
            self.continueFreeLabel.font = self.continueFreeLabel.font.withSize(iphone5AndLess ? 10 : 15)
        }

        self.continueFreeLabel.text = buttonTitle
        let closeBtnImg = crossType == .new ? "onboarding_close_new" : "onboarding_close"
        self.closeButton.setImage(UIImage(named: closeBtnImg), for: .normal)
    }
    
    func updateTitle(title: String, subtitle: String?, inappDescription: String, price: String) {
        UIView.animateKeyframes(withDuration: 0.6, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3, animations: {
                self.titleStackView.transform = .init(scaleX: 1.2, y: 1.2)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7, animations: {
                self.titleStackView.transform = .init(scaleX: 0.6, y: 0.6)
                self.titleStackView.alpha = 0
            })
        }) { (completed) in
            self.titleLabel.text = title
            self.secondSubtitleLabel.text = inappDescription
            if let subtitle = subtitle {
                self.subtitleLabel.text = String.init(format: subtitle, price)
            }
            self.titleStackView.transform = .init(scaleX: 1.6, y: 1.6)
            UIView.animateKeyframes(withDuration: 0.7, delay: 0, options: .calculationModeLinear, animations: {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.8, animations: {
                    self.titleStackView.transform = .init(scaleX: 0.8, y: 0.8)
                    self.titleStackView.alpha = 1
                })
                UIView.addKeyframe(withRelativeStartTime: 0.8, relativeDuration: 0.2, animations: {
                    self.titleStackView.transform = .identity
                })
            }, completion: nil)
        }
    }
    
    private var infoTextAttributes: (fontName: String, fontSize: CGFloat, align:String) {
        let isIpad = UIDevice.current.userInterfaceIdiom == .pad
        let defaultFont = UIFont.systemFont(ofSize: isIpad ? 17.5 : 11.5)
        return (defaultFont.fontName, defaultFont.pointSize, "justified")
    }
    
    private func calcTextViewHeight() {
        let frame = self.textView.attributedText.boundingRect(with: .init(width: self.textView.frame.width, height: .infinity), options: [.usesFontLeading, .usesLineFragmentOrigin], context: nil)
        
        let height = frame.size.height
        self.textViewHeight.constant = height * 1.3
    }
    
    @objc private func deviceRotated() {
        guard OnboardingUtils.isSerieXDevice() else { return }
        let currentOrientation = UIDevice.current.orientation
        if currentOrientation == .landscapeLeft {
            self.textViewLeadingSafeConstraint.isActive = true
            self.textViewLeadingViewConstraint.isActive = false
        } else if currentOrientation == .landscapeRight {
            self.textViewLeadingSafeConstraint.isActive = false
            self.textViewLeadingViewConstraint.isActive = true
        }
    }
    
    @IBAction func onContinueFree(_ sender: Any) {
        self.delegate?.continueFreeButtonDidTap()
    }
    
    func superViewDidLayoutSubviews() {
       self.calcTextViewHeight()
        disclamerVerticalOffsetConstraint.constant = self.frame.height - (isIpad ? 160 : 80)
    }

    func setAnimator(animator: OnboardingPageAnimator) {
        self.animator = animator
    }
}
