//
//  ImageOnboardingPage.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class ImageOnboardingPage: UIImageView, PageSide {
    
    var animationView: UIView {
        return self
    }

    var animator: OnboardingPageAnimator?
    lazy var stringsView = OnboardingStringsView(frame: self.bounds)
    private(set) var touchableSubviews: [TouchableView] = []
    
    func setup(imageName: String?) {
        guard let imgName = imageName else { return }
        let img = UIImage(named: imgName)
        self.contentMode = .scaleAspectFill
        self.image = img
        self.clipsToBounds = true
        self.addSubview(stringsView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.stringsView.frame = self.bounds
    }
    
    func superViewDidLayoutSubviews() {
    }
    
    func pageDidAppear() {
        self.animator?.startAnimation()
    }
    
    func pageDidDissapear() {
        self.animator?.prepare()
    }
    
    func actionButtonDidTapped() {
        self.stringsView.startAnimating()
    }
    
    func setAnimator(animator: OnboardingPageAnimator) {
        self.animator = animator
    }
    
}

