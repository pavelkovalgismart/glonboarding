//
//  PurchaseImgPage.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 07/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class PurchaseImgPage: CustomXibView, PageSide {
    var animationView: UIView {
        return self.animationSubview
    }

    var animator: OnboardingPageAnimator?
    lazy var stringsView = OnboardingStringsView(frame: self.bounds)
    
    @IBOutlet weak var trialDirationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var purchaseDescriptionContainer: UIView!
    @IBOutlet weak var animationSubview: UIView!
    @IBOutlet weak var priceDescriptionLabel: UILabel!
    private(set) var touchableSubviews: [TouchableView] = []
    @IBOutlet private  weak var image: UIImageView!
    @IBOutlet weak var restoreButton: UIButton!
    var restoreHandler: (() -> Void)?

    @IBOutlet weak var trialBlockBottomConstraint: NSLayoutConstraint!
    weak var nextButton: UIView?
    
    override func customize() {
        super.customize()
        self.image.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        self.contentView.insertSubview(self.stringsView, belowSubview: self.animationView)
        self.touchableSubviews.append(TouchableView(view: self.restoreButton,
                                                    superView: self, isOnScreen: {
                                                        return self.alpha != 0
        }))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.stringsView.frame = self.bounds
    }
    
    func setup(data: PurchasePageModel, price: String?) {
        if let imgName = data.imageName {
            let img = UIImage(named: imgName)
            self.image.image = img
        }
        
        if let price = price {
            self.purchaseDescriptionContainer.isHidden = false
            self.trialDirationLabel.text = data.trialDurationText
            if let priceDscr = data.inappDescription {
                self.priceDescriptionLabel.text = String(format: priceDscr, price)
            }
        } else {
            self.purchaseDescriptionContainer.isHidden = true
        }
        
        if let restoreTitle = data.restoreTitle {
            self.restoreButton.setTitle(restoreTitle, for: .normal)
        }
    }

    @IBAction func onRestoreClicked(_ sender: Any) {
        self.restoreHandler?()
    }
    
    func superViewDidLayoutSubviews() {
        self.restoreButton.layer.cornerRadius = self.restoreButton.frame.height / 2.0
        if let nextButton = self.nextButton {
            self.trialBlockBottomConstraint.constant = self.frame.height - nextButton.frame.minY + (OnboardingUtils.isIphone() ? 16 : 52)
        }
    }
    
    func actionButtonDidTapped() {
        self.stringsView.startAnimating()
    }
    
    func pageDidAppear() {
        self.animator?.startAnimation()
    }
    
    func pageDidDissapear() {
        self.animator?.prepare()
    }
    func setAnimator(animator: OnboardingPageAnimator) {
        self.animator = animator
    }
}
