//
//  PageEventsAnimator.swift
//  RealGuitar
//
//  Created by Nikolay Tsygankov on 23/07/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

protocol OnboardingPageAnimator {
    func startAnimation()
    func prepare()
}

class PageEventsAnimator: OnboardingPageAnimator {
    private var events = [UIView]()
    private let view: UIView
    
    init(view: UIView) {
        self.view = view
    }
    
    func startAnimation() {
        self.animateEvents()
    }
    
    func prepare() {
        self.events.forEach { $0.removeFromSuperview() }
    }
    
    private enum UI {
        static let isIphone = OnboardingUtils.isIphone()
        
        static let x0Offset: CGFloat = UI.isIphone ? -50/375.0 : -84/768.0
        static let x1Offset: CGFloat = UI.isIphone ? 23/365.0 : 33/768.0
        static let x2Offset: CGFloat = UI.isIphone ? 59/375.0 : 97/768.0
        
        static let y0Offset: CGFloat = UI.isIphone ? 0.6 : 0.5
        static let y1Offset: CGFloat = UI.isIphone ? 0.516 : 0.43
        static let y2Offset: CGFloat = UI.isIphone ? 0.684 : 0.57
        
        static let endY0Offset: CGFloat = UI.isIphone ? 145/375.0 : 260/768.0
        static let endY1Offset: CGFloat = UI.isIphone ? 68/375.0 : 135/768.0
        static let endY2Offset: CGFloat = UI.isIphone ? 182/375.0 : 342/768.0
        
        static let animationDuration: TimeInterval = 0.7
    }
    
    private func animateEvents() {
        let event0 = UIImageView(image: #imageLiteral(resourceName: "onboarding_event_1"))
        let event1 = UIImageView(image: #imageLiteral(resourceName: "onboarding_event_2"))
        let event2 = UIImageView(image: #imageLiteral(resourceName: "onboarding_event_3"))
        
        let w = self.view.frame.width
        let h = self.view.frame.height
        
        event0.center = .init(x: h * UI.x0Offset + w/2.0, y: UI.y0Offset * h)
        event1.center = .init(x: h * UI.x1Offset + w/2.0, y: UI.y1Offset * h)
        event2.center = .init(x: h * UI.x2Offset + w/2.0, y: UI.y2Offset * h)
        
        self.events = [event0, event1, event2]
        self.events.forEach {
            $0.alpha = 0
            self.view.addSubview($0)
            if OnboardingUtils.DeviceType.IS_IPHONE_5 {
                $0.transform = CGAffineTransform.init(scaleX: 320.0/375.0, y: 320.0/375.0)
            }
        }
        
        UIView.animate(withDuration: UI.animationDuration) {
            event0.center = .init(x: event0.center.x, y: UI.endY0Offset * h)
            event0.alpha = 1
        }
        
        UIView.animate(withDuration: UI.animationDuration, delay: 0.2, options: .curveEaseInOut, animations: {
            event1.center = .init(x: event1.center.x, y: UI.endY1Offset * h)
            event1.alpha = 1
        }, completion: nil)
        
        UIView.animate(withDuration: UI.animationDuration, delay: 0.4, options: .curveEaseInOut, animations: {
            event2.center = .init(x: event2.center.x, y: UI.endY2Offset * h)
            event2.alpha = 1
        }, completion: nil)
    }
}
