//
//  PageMediatorAnimator.swift
//  RealGuitar
//
//  Created by Nikolay Tsygankov on 23/07/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class PageMediatorAnimator: OnboardingPageAnimator{
    
    private let mediator = UIImageView(image: #imageLiteral(resourceName: "onboarding_mediator"))
    private let view: UIView
    
    private enum UI {
        static let isIphone = OnboardingUtils.isIphone()
        static let centerXAligment: CGFloat = 1.05
        static let animationDuration: TimeInterval = 0.5
        static let fromYOffset: CGFloat = UI.isIphone ? -0.1 : -0.07
        static let toYOffset0: CGFloat = UI.isIphone ? 0.26 : 0.182
        static let toYOffset1: CGFloat = UI.isIphone ? 0.2 : 0.14
        static let rotationAngle: CGFloat = -CGFloat.pi/16.0
    }
    
    init(view: UIView) {
        self.view = view
    }
    
    func startAnimation() {
        let w = self.view.frame.width
        let h = self.view.frame.height
        let xCenter = w/2.0 * UI.centerXAligment
        
        self.mediator.alpha = 0
        
        self.mediator.center = .init(x: xCenter, y: h * UI.fromYOffset)
        self.view.addSubview(self.mediator)
        
        //move
        UIView.animate(withDuration: UI.animationDuration, delay: 0, options: .curveEaseIn, animations: {
            self.mediator.alpha = 1
            self.mediator.center = .init(x: xCenter, y: h * UI.toYOffset0)
        }) { (completed) in
            UIView.animate(withDuration: UI.animationDuration, delay: 0, options: .curveEaseOut, animations: {
                self.mediator.center = .init(x: xCenter, y: h * UI.toYOffset1)
            }, completion: nil)
        }
        
        //rotate
        UIView.animate(withDuration: UI.animationDuration * 0.8, animations: {
            self.mediator.transform = CGAffineTransform.init(rotationAngle: UI.rotationAngle)
        }) { (completed) in
            UIView.animate(withDuration: UI.animationDuration * 1.4, animations: {
                self.mediator.transform = CGAffineTransform.identity
            })
        }
    }
    
    func prepare() {
        self.mediator.removeFromSuperview()
    }
}
