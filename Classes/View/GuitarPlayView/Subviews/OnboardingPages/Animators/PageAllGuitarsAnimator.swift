//
//  PageAllGuitarsAnimator.swift
//  RealGuitar
//
//  Created by Nikolay Tsygankov on 23/07/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class PageAllGuitarsAnimator: OnboardingPageAnimator {
    let imageView: UIImageView
    lazy var que = self.makeOperationQue()
    
    private enum UI {
        static let transitionDuration: TimeInterval = 0.7
        static let delay: TimeInterval = 1.2
    }
    
    init(view: UIView) {
        if let imageView = view as? UIImageView {
            self.imageView = imageView
        } else {
            self.imageView = UIImageView()
            view.addSubview(self.imageView)
        }
    }
    
    func startAnimation() {
        let imageIndexes = [3, 4, 2]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            for _ in 1...20 {
                imageIndexes.forEach { (index) in
                    self.que.addOperation {
                        DispatchQueue.main.async {
                            UIView.transition(with: self.imageView, duration: UI.transitionDuration, options: .transitionCrossDissolve, animations: {
                                self.imageView.image = UIImage(named: "onboarding_img_" + String(index))
                            }, completion: nil)
                        }
                        Thread.sleep(forTimeInterval: UI.delay)
                    }
                }
            }
        }
    }
    
    func prepare() {
        self.que.cancelAllOperations()
        self.imageView.layer.removeAllAnimations()
        self.imageView.image = UIImage(named: "onboarding_img_2")
    }
    
    //MARK: Private
    private func makeOperationQue() -> OperationQueue{
        let que = OperationQueue()
        que.maxConcurrentOperationCount = 1
        return que
    }
    
}
