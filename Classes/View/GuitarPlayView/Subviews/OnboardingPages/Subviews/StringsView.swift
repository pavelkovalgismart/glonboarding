//
//  StringsView.swift
//  RealGuitar
//
//  Created by Nikolay Tsygankov on 23/07/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class OnboardingStringsView: UIView {
    
    private enum Animation {
        static let duration: TimeInterval = 0.7
        static let rltvFadeDuration: Double = 0.1
    }
    
    lazy var strings = self.makeStrings()
    lazy var playingStrings = self.makePlayingStrings()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private enum UI {
        static let stringAreaToHeightRelation: CGFloat = OnboardingUtils.isIphone() ? 182/375.0 : 310/768.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutStrings()
    }
    
    //MARK: Public
    func startAnimating() {
        let delay = 0.075
        for i in 0..<self.strings.count {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay * Double(i)) {
                self.animateString(index: i)
            }
        }
    }
    
    //MARK: Private
    private func makeStrings() -> [UIView] {
        var strings = [UIView]()
        guard let boldStringImg = UIImage(name: "onboarding_bold_string_pattern"),
            let narrowStringImg = UIImage(name: "onboarding_narrow_string_pattern") else { return strings }
        for i in 0..<6 {
            let stringImg = i > 2 ? narrowStringImg : boldStringImg
            let color = UIColor.init(patternImage: stringImg)
            let string = UIView()
            string.backgroundColor = color
            string.frame = .init(origin: .zero, size: .init(width: stringImg.size.width, height: self.frame.height))
            strings.append(string)
            self.addSubview(string)
        }
        return strings
    }
    
    private func makePlayingStrings() -> [UIView]{
        var strings = [UIView]()
        guard let playingStringImg = UIImage(name:"onboarding_playing_string_pattern") else { return strings }
        for _ in 0..<6 {
            let playingString = UIView()
            let playingStringColor = UIColor.init(patternImage: playingStringImg)
            playingString.backgroundColor = playingStringColor
            playingString.frame = .init(origin: .zero, size: .init(width: playingStringImg.size.width, height: self.frame.height))
            playingString.alpha = 0
            strings.append(playingString)
            self.addSubview(playingString)
        }
        return strings
    }
    
    private func layoutStrings() {
        for i in 0..<self.strings.count {
            let string = self.strings[i]
            let stringArea = self.frame.height * UI.stringAreaToHeightRelation
            string.frame = .init(origin: .zero, size: .init(width: string.frame.width, height: self.frame.height))
            let spacing = stringArea / CGFloat(self.strings.count - 1)
            string.center = CGPoint(x: (self.frame.width - stringArea)/2.0 + CGFloat(i) * spacing * 1.03, y: self.frame.height/2.0)
            
            let playingString = self.playingStrings[i]
            playingString.frame = .init(origin: .zero, size: .init(width: playingString.frame.width, height: self.frame.height))
            playingString.center = CGPoint(x: string.center.x, y: self.frame.height/2.0)
        }
    }
    
    private func animateString(index: Int) {
        let string = self.strings[index]
        UIView.animateKeyframes(withDuration: Animation.duration, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: Animation.rltvFadeDuration, animations: {
                string.alpha = 0
            })
            UIView.addKeyframe(withRelativeStartTime: 0.9, relativeDuration: Animation.rltvFadeDuration, animations: {
                string.alpha = 1
            })
        })
        
        let playingString = self.playingStrings[index]
        UIView.animateKeyframes(withDuration: Animation.duration, delay: 0, options: .calculationModeLinear, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: Animation.rltvFadeDuration, animations: {
                playingString.alpha = 0.2
            })
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 1-2*Animation.rltvFadeDuration, animations: {
                playingString.alpha = 1
            })
            UIView.addKeyframe(withRelativeStartTime: 0.1, relativeDuration: 1-Animation.rltvFadeDuration, animations: {
                playingString.transform = CGAffineTransform.init(scaleX: string.frame.width/playingString.frame.width, y:1 )
            })
            UIView.addKeyframe(withRelativeStartTime: 0.9, relativeDuration: Animation.rltvFadeDuration, animations: {
                playingString.alpha = 0
            })
        }) { (completed) in
            playingString.transform = .identity
        }
    }
}
