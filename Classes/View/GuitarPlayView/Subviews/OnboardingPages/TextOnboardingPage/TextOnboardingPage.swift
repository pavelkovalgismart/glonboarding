//
//  TextOnboardingPage.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class TextOnboardingPage: CustomXibView, PageSide {
    var animationView: UIView {
        return self
    }

    func setAnimator(animator: OnboardingPageAnimator) {
        self.animator = animator
    }
    
    var animator: OnboardingPageAnimator?
    
    private(set) var touchableSubviews: [TouchableView] = []
    
    @IBOutlet weak var bodyOnboardingPage: UILabel!
    @IBOutlet weak var titleOnboardingPage: UILabel!
    
    func setup(title: String?, body: String?) {
        self.titleOnboardingPage.text = title
        self.bodyOnboardingPage.text = body
    }
    
    func superViewDidLayoutSubviews() {
    }
    
}
