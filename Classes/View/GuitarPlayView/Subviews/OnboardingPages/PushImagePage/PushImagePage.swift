//
//  PushImagePage.swift
//  RealDrumFree
//
//  Created by Nikolay Tsygankov on 19/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class PushImagePage: CustomXibView, PageSide {
    var animationView: UIView {
        return self
    }

    var animator: OnboardingPageAnimator?
    
    private(set) var touchableSubviews: [TouchableView] = []
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pushDescriptionLabel: UILabel!
    lazy var allowLabel: UILabel = self.makeLabel()
    
    
    func setup(imageName: String?, pushDescription: String?, allowButtonText: String?) {
        guard let imgName = imageName else { return }
        let img = UIImage(named: imgName)
        self.imageView.image = img
        self.allowLabel.text = allowButtonText
        self.pushDescriptionLabel.text = pushDescription
    }
    
    func superViewDidLayoutSubviews() {
        self.setupFrames()
    }
    
    func pageDidAppear() {
    }
    
    private func setupFrames() {
        self.allowLabel.frame = .init(origin: .init(x: imageView.bounds.midX + 5, y: 95/165 * imageView.bounds.height),
                                      size: CGSize(width: imageView.bounds.width/2 - 10, height: 44/165 * imageView.bounds.height))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setupFrames()
    }
    
    func setAnimator(animator: OnboardingPageAnimator) {
        self.animator = animator
    }
    
    //MARK: Private
    private func makeLabel() -> UILabel {
        let lbl = UILabel()
        lbl.text = "ALLOW"
        let isIpad = UIDevice.current.userInterfaceIdiom == .pad
        lbl.font = UIFont.systemFont(ofSize: isIpad ? 22 : 14, weight: .heavy)
        lbl.textColor = UIColor.orange
        lbl.textAlignment = .center
        self.imageView.addSubview(lbl)
        return lbl
    }
}
