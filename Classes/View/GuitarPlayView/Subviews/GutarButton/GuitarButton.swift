//
//  GuitarButton.swift
//  RealGuitar
//
//  Created by Alex on 5/22/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class GuitarButton: UIButton {
    
    private enum UI {
        static let isIpad = UIDevice.current.userInterfaceIdiom == .pad
        static let verticalAnimationOffset: CGFloat = 10
        static let arrowImageOffset: CGFloat = isIpad ? 16 : 8
        static let btnLeftContentOffsetRatio: CGFloat = 12/306.0
        static let btnRightContentOffsetRatio: CGFloat = 18/306.0
    }
    
    private var soundManager = OnboardingSoundManager()
    private var arrowImageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customize()
    }
    
    private func customize() {
        let arrowImageView = UIImageView(image: UIImage(named: "next_button_icon"))
        self.arrowImageView = arrowImageView
        self.addSubview(arrowImageView)
        
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.1
        self.titleLabel?.numberOfLines = 2
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.layer.shadowOffset = .init(width: 0, height: 1)
        self.titleLabel?.layer.shadowColor = UIColor.black.withAlphaComponent(0.24).cgColor
        self.titleLabel?.layer.shadowOpacity = 1
        self.titleLabel?.layer.shadowRadius = 1
        
        self.adjustsImageWhenHighlighted = false
        
        self.clipsToBounds = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let labelFrame = self.titleLabel?.frame {
            self.arrowImageView?.center = CGPoint(x: labelFrame.maxX + UI.arrowImageOffset, y: labelFrame.midY)
        }
        self.setupContentInsets()
    }
    
    //MARK: Private
    private func setupContentInsets() {
        self.contentEdgeInsets = .init(top: 0,
                                       left: UI.btnLeftContentOffsetRatio * self.frame.width,
                                       bottom: 0,
                                       right: UI.btnRightContentOffsetRatio * self.frame.width)
    }
    
    private func addButtonAnimation() {
        let animationDuration: TimeInterval = 1.5
        
        let verticalAnimation = CAKeyframeAnimation(keyPath: "position.y")
        let fromY = self.layer.position.y
        verticalAnimation.values = [fromY, fromY - UI.verticalAnimationOffset, fromY,  fromY - UI.verticalAnimationOffset, fromY]
        verticalAnimation.keyTimes = [0, 0.1, 0.2, 0.3, 0.4]
        verticalAnimation.duration = animationDuration
        verticalAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.values = [0, Double.pi/92, 0 , -Double.pi/92, 0]
        rotateAnimation.keyTimes = [0, 0.1, 0.2, 0.3, 0.4]
        rotateAnimation.duration = animationDuration
        rotateAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let groupAnimation = CAAnimationGroup()
        groupAnimation.duration = 2 * animationDuration
        groupAnimation.beginTime = CACurrentMediaTime() + animationDuration
        groupAnimation.animations = [rotateAnimation, verticalAnimation]
        groupAnimation.repeatCount = .infinity
        
        self.layer.add(groupAnimation, forKey: nil)
    }
    
    private func addScaleOnTapAnimation() {
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.values = [1, 0.88, 1, 1, 0.88, 1, 1]
        scaleAnimation.keyTimes = [0, NSNumber.init(value: 5/22.0) , NSNumber.init(value: 7/22.0), NSNumber.init(value: 8/22.0), NSNumber.init(value: 12/22.0), NSNumber.init(value: 18/22.0), 1]
        scaleAnimation.duration = 22/30.0
        scaleAnimation.beginTime = CACurrentMediaTime()
        self.layer.add(scaleAnimation, forKey: nil)
    }
}

extension GuitarButton: OnboardingButton {
    func setTitle(_ title: String?, lastPage: Bool) {
        self.arrowImageView?.isHidden = lastPage
        self.titleLabel?.font = UIFont.systemFont(ofSize: !UI.isIpad ? 15 : 23, weight: UIFont.Weight.heavy)
        self.setTitle(title, for: .normal)
    }
    
    func setTitle(_ title: String?, priceDescr: String?, showArrow: Bool, animated: Bool) {
        self.arrowImageView?.isHidden = showArrow

        var attrTitle: NSMutableAttributedString?
        let isIphone = !UI.isIpad
        let fontColor = UIColor.white

        if let title = title {
            attrTitle = NSMutableAttributedString(
                string: title,
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: isIphone ? 15 : 23, weight: UIFont.Weight.heavy),
                    NSAttributedString.Key.foregroundColor : fontColor
                ]
            )
        }

        if let attrTitle = attrTitle,
            let descr = priceDescr {
            attrTitle.append(NSMutableAttributedString(
                string: "\n" + descr,
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: isIphone ? 12 : 20, weight: UIFont.Weight.regular),
                    NSAttributedString.Key.foregroundColor : fontColor
                ]
            ))
        }

        self.setAttributedTitle(attrTitle, for: .normal)
    }
    
    func startAnimation() {
    }

    func didTouchUpInside(from page: Int) {
        self.addScaleOnTapAnimation()
        if #available(iOS 10.0, *) {
            let feedback = UISelectionFeedbackGenerator()
            feedback.selectionChanged()
        }
        self.soundManager.playSound(name: "onboarding_button_" + String(page), type: "mp3")
    }
    
    func setupForView(_ view: UIView) {
        guard let buttonImage = UIImage(named: "onboarding_button") else { return }
        self.setBackgroundImage(buttonImage, for: .normal)
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = UIFont.systemFont(ofSize: OnboardingUtils.isIphone() ? 20.0 : 34.0, weight: .heavy)

        let botSpacingView = UIView()
        botSpacingView.backgroundColor = UIColor.clear
        view.addSubview(botSpacingView)
        botSpacingView.translatesAutoresizingMaskIntoConstraints = false
        botSpacingView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        botSpacingView.topAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        botSpacingView.widthAnchor.constraint(equalToConstant: 0).isActive = true
        botSpacingView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: OnboardingUtils.isIphone() ? (37/375.0) : isIpadPro11 ? (86/768.0) : (104/768.0)).isActive = true
        botSpacingView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.widthAnchor.constraint(equalTo: view.heightAnchor, multiplier: OnboardingUtils.isIphone() ? 228.0 / 375.0 : 380.0 / 768.0 ).isActive = true
        self.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: OnboardingUtils.isIphone() ? 74.0 / 375.0 : 101.0 / 768.0 ).isActive = true
    }
    
    func stopSound() {
        self.soundManager.player?.stop()
    }
}
