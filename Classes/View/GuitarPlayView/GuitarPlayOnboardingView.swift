//
//  OnboardingView.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

protocol PageSide {
    var touchableSubviews: [TouchableView] {get}
    func superViewDidLayoutSubviews()
    func setAnimator(animator: OnboardingPageAnimator)
    func pageDidAppear()
    func pageDidDissapear()
    func actionButtonDidTapped()
    var animationView: UIView { get }
}

extension PageSide {
    func pageDidAppear() {}
    func pageDidDissapear() {}
    func actionButtonDidTapped() {}
}

struct OnboardingPage {
    let leftPart: UIView & PageSide
    let rightPart: UIView & PageSide
    
    func pageDidAppear() {
        self.leftPart.pageDidAppear()
        self.rightPart.pageDidAppear()
    }
    
    func pageDidDissapear() {
        self.leftPart.pageDidDissapear()
        self.rightPart.pageDidDissapear()
    }
}

class GuitarPlayOnboardingView: UIViewController, OnboardingView {
    
    
    var presenter: OnboardingPresenter?
    weak var delegate: OnboardingViewDelegate?
    weak var dataSource: OnboardingViewDataSource?
    @IBOutlet weak var scrollView: PassThroughScrollView!
    @IBOutlet weak var rightSide: UIView!
    @IBOutlet weak var rightSideBackground: UIImageView!
    lazy var activityView: UIActivityIndicatorView = self.makeActivity()
    var askAgainController: AskAgainNotificationViewController?
    private var pages: [OnboardingPage] = []
    private var scrollInProgress: Bool = false
    private var prevPage: Int?
    private var currentPage: Int = 0 {
        didSet {
            if oldValue != currentPage {
                prevPage = oldValue
                self.delegate?.currentPageChanged(to: currentPage, fromPageIndex: oldValue, slide: !self.scrollInProgress)
                self.pages[self.currentPage].rightPart.pageDidAppear()
                if let prevPage = self.prevPage {
                    self.pages[prevPage].pageDidDissapear()
                }
            }
        }
    }
    
    private var currentRatio: CGFloat = 0 {
        didSet {
            guard self.currentPage == 1 else { return }
            let isLeftScrollDirection = oldValue > currentRatio
            if isLeftScrollDirection {
                self.rightSideBackground.image = UIImage(name: "onboarding_img_1")
            } else {
                self.rightSideBackground.image = UIImage(name: "onboarding_img_4")
            }
        }
    }
    
    enum UI {
        static let pageSpacing: CGFloat = 40
        static let scrollDuration: TimeInterval = 1.6
    }
    
    var nextButton: (UIButton & OnboardingButton)!
    
    //MARK: - View Lifcycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate?.viewDidLoad()
        self.scrollView.delegate = self
        self.setupNexButton()
        self.buildPages()
        self.pages[0].leftPart.alpha = 1
        self.pages[0].rightPart.alpha = 1
        self.scrollView.touchableViews.append(TouchableView(view: self.nextButton, superView: self.rightSide, isOnScreen: {
            return true
        }))
        self.pages.forEach { (page) in
            [page.leftPart, page.rightPart].forEach({ (pageSide) in
                self.scrollView.touchableViews.append(contentsOf: pageSide.touchableSubviews)
            })
        }
        
        self.view.bringSubviewToFront(self.scrollView)
        self.rightSide.bringSubviewToFront(nextButton)
        self.askAgainController = GuitarAskAgainNotificationViewController.create()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.nextButton.startAnimation()
        self.pages.first?.pageDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
        self.nextButton.stopSound()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let countOfPages = self.dataSource?.countOfPages() else { return }
        
        let width = self.view.frame.width
        let height = self.view.frame.height
        let halfWidth = self.view.frame.width / 2
        
        self.scrollView.contentSize = CGSize(width: CGFloat(countOfPages) * width, height: height)
        
        //setup page Sizes
        self.pages.forEach { (page) in
            [page.leftPart, page.rightPart].forEach {
                var frame = $0.frame
                frame.origin = .zero
                frame.size = CGSize.init(width: halfWidth, height: height)
                $0.frame = frame
                $0.superViewDidLayoutSubviews()
            }
        }
    }
    
    //MARK: Private
    private func setupNexButton() {
        self.nextButton = GuitarButton()
        self.rightSide.addSubview(nextButton)
        nextButton.addTarget(self, action:#selector(self.onActionButtonClicked), for: .touchUpInside)
        nextButton.setupForView(self.rightSide)
        self.setButtonTitle(pageIndex: 0)
	self.nextButton.accessibilityIdentifier = "nextButton"
    }
    
    private func buildPages() {
        guard let pagesCount = self.dataSource?.countOfPages() else { return }
        for i in 0...pagesCount {
            if let page = self.makePageView(at: i) {
                self.pages.append(page)
                page.leftPart.alpha = 0
                page.rightPart.alpha = 0
                page.leftPart.layer.anchorPoint = .zero
                page.rightPart.layer.anchorPoint = .zero
                self.view.addSubview(page.leftPart)
                self.rightSide.addSubview(page.rightPart)
            }
        }
        
        //add Animators
        self.pages[0].rightPart.setAnimator(animator: PageEventsAnimator(view: self.pages[0].rightPart.animationView))
        self.pages[1].rightPart.setAnimator(animator:  PageAllGuitarsAnimator(view: self.pages[1].rightPart.animationView))
        self.pages[2].rightPart.setAnimator(animator: PageMediatorAnimator(view: self.pages[2].rightPart.animationView))
    }
    
    private func animateAppearing() {
        self.scrollView.contentOffset = .init(x: -self.view.frame.width, y: 0)
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.96, 0, 0.05, 1)
        self.scrollView.setContentOffset(.zero, with: timingFunction, duration: UI.scrollDuration)
    }
    
    private func setButtonTitle(pageIndex: Int) {
        let buttonText = self.dataSource?.nextButtonText(forPageAt: pageIndex)
        let isLastPage = (self.pages.count - 1) == pageIndex
        self.nextButton.setTitle(buttonText, lastPage: isLastPage)
    }
    
    private func makePageView(at index: Int) -> OnboardingPage? {
        guard let pageModel = self.dataSource?.onboardingPage(at: index) else { return nil }
        let pageSideSize = self.rightSide.frame.size
        switch pageModel.type {
        case .normal:
            let normalPageData = pageModel as! NormalPageModel
            let left = TextOnboardingPage(frame: .init(origin: .zero, size: pageSideSize))
            left.setup(title: normalPageData.title, body: normalPageData.body)
            let right = ImageOnboardingPage(frame: .init(origin: .zero, size: pageSideSize))
            right.setup(imageName: normalPageData.image)
            return OnboardingPage(leftPart: left, rightPart: right)
        case .purchase:
            let inappPrice = self.dataSource?.inappPrice()
            let purchasePageData = pageModel as! PurchasePageModel
            let left = PurchaseTextPage(frame: .init(origin: .zero, size: pageSideSize))
            left.setup(title: purchasePageData.title,
                       subtitle: purchasePageData.subtitle,
                       bodyStrings: purchasePageData.bodyStrings,
                       subscriptionPrice: inappPrice,
                       policyUrl: purchasePageData.policyUrl,
                       termUrlString: purchasePageData.termsUrl,
                       termsInfo: purchasePageData.termsInfoText,
                       buttonTitle: purchasePageData.continueFreeButtonText,
                       inappDescription: purchasePageData.inappDescription,
                       crossType: purchasePageData.crossType)
            left.delegate = self
            let right = PurchaseImgPage(frame: .init(origin: .zero, size: pageSideSize))
            right.setup(data: purchasePageData, price: inappPrice)
            right.nextButton = self.nextButton
            right.restoreHandler = { [weak self ] in
                self?.delegate?.restoreAction()
            }
            return OnboardingPage(leftPart: left, rightPart: right)
        case .pushNotification:
            let pageData = pageModel as! PushNotificationPageModel
            let left = TextOnboardingPage(frame: .init(origin: .zero, size: pageSideSize))
            left.setup(title: pageData.title, body: pageData.body)
            let right = PushImagePage(frame: .init(origin: .zero, size: pageSideSize))
            right.setup(imageName: pageData.image, pushDescription: pageData.pushDescription, allowButtonText: pageData.allowButtonTitle)
            return OnboardingPage(leftPart: left, rightPart: right)
        }
    }
    
    private func makeActivity() -> UIActivityIndicatorView {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.hidesWhenStopped = true
        activity.frame = .init(x: 0, y: 0, width: 40, height: 40)
        activity.center = self.view.center
        self.view.addSubview(activity)
        return activity
    }
    
    private func scrollToNextPage() {
        guard self.currentPage < self.pages.count - 1 else { return }
        let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        let contentOffset = CGPoint.init(x: self.view.frame.width * CGFloat(self.currentPage + 1), y: 0)
        self.scrollView.setContentOffset(contentOffset, with: timingFunction, duration: UI.scrollDuration)
        self.view.isUserInteractionEnabled = false
        self.scrollInProgress = true
        DispatchQueue.main.asyncAfter(deadline: .now() + UI.scrollDuration * 1.1) {
            self.scrollInProgress = false
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK: OnboardingView
    func close(completion: (() -> Void)?) {
        self.dismiss(animated: true, completion: completion)
    }
    
    func showActivity(show: Bool) {
        if show {
            self.activityView.startAnimating()
            self.view.isUserInteractionEnabled = false
        } else {
            self.activityView.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func showCancelPaymentPopup(viewData: AskAgainModel, okAction: @escaping (() -> Void), cancelAction: @escaping (() -> Void)) {
        guard let askAgainPopup = self.askAgainController else {
            return
        }
        askAgainPopup.show(from: self, viewData: viewData)
        askAgainPopup.okHandler = {
            askAgainPopup.dismissSelf(completion: okAction)
            
        }
        askAgainPopup.cancelHandler = {
            askAgainPopup.dismissSelf(completion: cancelAction)
        }
    }
    
    func updatePurchasePageData() {
        guard
            self.pages.count > 0,
            let pageModel = self.dataSource?.onboardingPage(at: self.pages.count - 1) as? PurchasePageModel
            else { return }
        
        let price = self.dataSource?.inappPrice()
        if let left = self.pages.last?.leftPart as? PurchaseTextPage {
            left.setup(title: pageModel.title,
                       subtitle: pageModel.subtitle,
                       bodyStrings: pageModel.bodyStrings,
                       subscriptionPrice: price,
                       policyUrl: pageModel.policyUrl,
                       termUrlString: pageModel.termsUrl,
                       termsInfo: pageModel.termsInfoText,
                       buttonTitle: pageModel.continueFreeButtonText,
                       inappDescription: pageModel.inappDescription,
                       crossType: pageModel.crossType)
        }
        
        if let right = self.pages.last?.rightPart as? PurchaseImgPage {
            right.setup(data: pageModel, price: price)
        }
        
        self.setButtonTitle(pageIndex: self.currentPage)
    }

    func showMessage(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Localizations.Ok, style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    func closeButton(isHidden: Bool) {
    }
    
    //MARK: Actions
    @objc func onActionButtonClicked(_ sender: Any) {
        guard let pageModel = self.dataSource?.onboardingPage(at: self.currentPage) else { return }
        self.nextButton.didTouchUpInside(from: self.currentPage)
        self.pages[currentPage].rightPart.actionButtonDidTapped()
        switch pageModel.type {
        case .normal:
            self.scrollToNextPage()
        case .purchase:
            self.delegate?.purchaseButtonDidTap()
        case .pushNotification:
            self.scrollToNextPage()
        }
    }

    func goToShopPage() {
        let lastPageIndex = self.pages.count - 1
        guard self.currentPage != lastPageIndex else {
            return
        }
        self.currentPage = lastPageIndex
        let contentOffset = CGPoint(x: self.view.frame.width * CGFloat(self.currentPage), y: 0)
        self.scrollView.setContentOffset(contentOffset, with: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut), duration: 0.2)
        
        self.view.isUserInteractionEnabled = true
        self.scrollInProgress = false
    }
}

//MARK: UIScrollViewDelegate
extension GuitarPlayOnboardingView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let ratio = scrollView.contentOffset.x / scrollView.frame.width
        let ratio0_1 = ratio.truncatingRemainder(dividingBy: 1)
        self.currentRatio = ratio
        if ratio0_1 == 0  {
            self.currentPage = Int(ratio + 0.5)
            self.setButtonTitle(pageIndex: self.currentPage)
            return
        }
        let firstAnimPage = Int(ratio)
        let secondAnimPage = firstAnimPage + 1
        guard secondAnimPage < self.pages.count else { return }
        
        if (ratio0_1 <= 0.5) {
            self.setPositionToPage(at: firstAnimPage, ratio: ratio0_1)
            self.setAlphaToPage(at: firstAnimPage, ratio: ratio0_1)
            self.setAlphaToPage(at: secondAnimPage, ratio: 0.5)
        } else {
            self.setAlphaToPage(at: firstAnimPage, ratio: 0.5)
            self.setPositionToPage(at: secondAnimPage, ratio: ratio0_1)
            self.setAlphaToPage(at: secondAnimPage, ratio: ratio0_1)
        }
    }
    
    private func setPositionToPage(at index: Int, ratio: CGFloat) {
        let page = self.pages[index]
        var xPos: CGFloat = 1
        if (ratio > 0.5) {
            xPos = 2 * UI.pageSpacing - (2 * UI.pageSpacing * ratio)
        } else {
            xPos = -(2 * ratio * UI.pageSpacing)
        }
        page.leftPart.layer.position = CGPoint.init(x: xPos, y: 0)
    }
    
    private func setAlphaToPage(at index: Int, ratio: CGFloat) {
        let ratio = Float(ratio)
        let page = self.pages[index]
        var alphaVal: Float = 1
        if (ratio > 0.5) {
            alphaVal =  (2 * ratio) - 1
        } else {
            alphaVal = -(2 * ratio) + 1
        }
        
        page.leftPart.layer.opacity = alphaVal
        
        if alphaVal > 0.5 {
            alphaVal = 1
        } else {
            alphaVal = 2 * alphaVal
        }
        page.rightPart.layer.opacity = alphaVal
    }
}

//MARK: PurchaseImgPageDelegate
extension GuitarPlayOnboardingView: PurchaseTextPageDelegate {
    func closeAction() {
        self.delegate?.closeAction()
    }
    
    func continueFreeButtonDidTap() {
        self.delegate?.continueFreeButtonDidTap()
    }
}
