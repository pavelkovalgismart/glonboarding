//
//  UIImage+Gradient.swift
//  RealGuitarLite
//
//  Created by Alex on 4/29/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import Foundation

struct GradientPoint {
    var location: CGFloat
    var color: UIColor
}

extension UIImage {
    convenience init?(size: CGSize, gradientPoints: [GradientPoint]) {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        guard let gradient = CGGradient(colorSpace: CGColorSpaceCreateDeviceRGB(),
                                        colorComponents: gradientPoints.compactMap { $0.color.cgColor.components }.flatMap { $0 },
                                        locations: gradientPoints.map { $0.location },
                                        count: gradientPoints.count)
            else {
                return nil
        }
        
        context.drawLinearGradient(gradient, start: CGPoint.zero, end: CGPoint(x: size.width, y: 0), options: CGGradientDrawingOptions())
        guard let image = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
        self.init(cgImage: image)
        defer { UIGraphicsEndImageContext() }
    }
}
