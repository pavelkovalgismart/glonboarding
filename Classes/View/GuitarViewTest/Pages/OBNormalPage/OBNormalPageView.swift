//
//  OBPage1.swift
//  RealGuitarLite
//
//  Created by Alex on 4/24/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import UIKit

class OBNormalPageView: CustomXibView {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titlesStackView: UIStackView!
    
    @IBOutlet weak var titlesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var rightPartTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var safeAreaTrailingConstraint: NSLayoutConstraint!
    
    override func customize() {
        super.customize()
        
        // Default iPhone X
        var titleFontSize: CGFloat = 28.0
        var subtitleFontSize: CGFloat = 14.0
        var spacing: CGFloat = 11.0
        var topConstraint: CGFloat = 79.0
        var trailingConstraint: CGFloat = 32.0
        
        if OnboardingUtils.DeviceType.IS_IPHONE_5 {
            titleFontSize = 26.0
            subtitleFontSize = 13.0
            spacing = 7.0
            topConstraint = 75.0
            trailingConstraint = 16.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_6 {
            trailingConstraint = 32.0
        }
        
        if !OnboardingUtils.isIphone() {
            titleFontSize = 42.0
            subtitleFontSize = 21.0
            spacing = 16.0
            topConstraint = 253.0
            trailingConstraint = 50.0
        }
        
        titleLabel.font = titleLabel.font.withSize(titleFontSize)
        subtitleLabel.font = subtitleLabel.font.withSize(subtitleFontSize)
        titlesTopConstraint.constant = topConstraint
        titleTrailingConstraint.constant = trailingConstraint
        titlesStackView.spacing = spacing
        
        if #available(iOS 11.0, *) {
            safeAreaTrailingConstraint.constant = UIApplication.shared.keyWindow?.safeAreaInsets.right ?? 0.0
        }
    }
}

extension OBNormalPageView: OBPage {
    
    func setupWithModel(model: PageModel) {
        guard let model = model as? NormalPageModel else { return }
        titleLabel.text = model.title
        subtitleLabel.text = model.body
        backgroundImageView.image = UIImage(name: model.image)
    }
    
    func superViewDidLayoutSubviews() {
        addGradientForTitile()
    }
    
    func fadeInAnimationWithProgress(progress: CGFloat) {
        backgroundImageView.alpha = progress
        rightPartTrailingConstraint.constant = frame.width / 2 * (progress - 1)
    }
    
    func fadeOutAnimationWithProgress(progress: CGFloat) {
        backgroundImageView.alpha = (1 - progress * 2)
        rightPartTrailingConstraint.constant = frame.width / 2 * progress
    }
    
}

private extension OBNormalPageView {
    
    func addGradientForTitile() {
        let points = [GradientPoint(location: 0, color: UIColor(r: 255, g: 124, b: 5, a: 1.0)),
                      GradientPoint(location: 0.9, color: UIColor(r: 255, g: 174, b: 5, a: 1.0))]
        if let gradientImage = UIImage(size: titleLabel.frame.size, gradientPoints: points) {
            let color = UIColor(patternImage: gradientImage)
            titleLabel.textColor = color
        }
    }
}
