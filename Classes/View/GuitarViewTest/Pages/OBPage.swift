//
//  OBPage.swift
//  RealGuitarLite
//
//  Created by Alex on 5/15/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import Foundation

protocol OBPage {
    func fadeInAnimationWithProgress(progress: CGFloat)
    func fadeOutAnimationWithProgress(progress: CGFloat)
    func setupWithModel(model: PageModel)
    func superViewDidLayoutSubviews()
}
