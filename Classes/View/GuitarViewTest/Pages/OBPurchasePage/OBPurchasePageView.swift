//
//  OBPage2.swift
//  RealGuitarLite
//
//  Created by Alex on 4/24/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import UIKit

protocol OBPurchasePageDelegate: class {
    func restoreAction()
    func closeAction()
}

class OBPurchasePageView: CustomXibView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var pointLabels: [UILabel]!
    @IBOutlet weak var pointsStackView: UIStackView!
    @IBOutlet weak var trialDescriptionLabel: UILabel!
    @IBOutlet weak var priceDescriptionLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var restoreButton: UIButton!
    
    //@IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rightPartTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var safeAreaTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pointsTopConstraint: NSLayoutConstraint!
    var delegate: OBPurchasePageDelegate?
    private var infoTextAttributes: (fontName: String, fontSize: CGFloat, align:String) {
        let isIpad = UIDevice.current.userInterfaceIdiom == .pad
        let defaultFont = UIFont.systemFont(ofSize: isIpad ? 17.5 : 11.5)
        return (defaultFont.fontName, defaultFont.pointSize, "justified")
    }
    
    @IBAction func onRestoreClicked(_ sender: Any) {
       delegate?.restoreAction()
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        delegate?.closeAction()
    }
    
    override func customize() {
        super.customize()
        
        // Default iPhone X
        var titleFontSize: CGFloat = 28.0
        var pointLabelsFontSize: CGFloat = 14.0
        var spacing: CGFloat = 4.0
        var topConstraint: CGFloat = 32.0
        var descriptionBottom: CGFloat = 130.0
        var trailingConstraint: CGFloat = 32.0
        var descriptionLabelsFontSize: CGFloat = 13.0
        var textViewFontSize: CGFloat = 13.0
        var pointsTopConstant: CGFloat = 8.0
        
        if OnboardingUtils.DeviceType.IS_IPHONE_5 {
            titleFontSize = 26.0
            pointLabelsFontSize = 13.0
            spacing = 2.0
            topConstraint = 24.0
            trailingConstraint = 16.0
            descriptionLabelsFontSize = 11.0
            textViewFontSize = 11.0
            descriptionBottom = 100.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_6 {
            trailingConstraint = 32.0
            descriptionBottom = 130.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_6P {
            topConstraint = 60.0
            descriptionBottom = 140.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_XR_OR_XS_MAX {
            topConstraint = 50.0
            descriptionBottom = 140.0
        }
        
        if !OnboardingUtils.isIphone() {
            titleFontSize = 42.0
            pointLabelsFontSize = 21.0
            descriptionLabelsFontSize = 19.0
            spacing = 8.0
            topConstraint = 140.0
            trailingConstraint = 50.0
            textViewFontSize = 19.0
            descriptionBottom = 250.0
            pointsTopConstant = 100.0
        }
        
        // Fonts
        titleLabel.font = titleLabel.font.withSize(titleFontSize)
        pointLabels.forEach{ $0.font = $0.font.withSize(pointLabelsFontSize) }
        trialDescriptionLabel.font = trialDescriptionLabel.font.withSize(descriptionLabelsFontSize)
        priceDescriptionLabel.font = priceDescriptionLabel.font.withSize(descriptionLabelsFontSize)
        textView.font = textView.font?.withSize(textViewFontSize)
        
        // Constraints
        //titleTopConstraint.constant = topConstraint
        descriptionBottomConstraint.constant = descriptionBottom
        titleTrailingConstraint.constant = trailingConstraint
        pointsStackView.spacing = spacing
        pointsTopConstraint.constant = pointsTopConstant
        
        if #available(iOS 11.0, *) {
            safeAreaTrailingConstraint.constant = UIApplication.shared.keyWindow?.safeAreaInsets.right ?? 0.0
        }
        
        restoreButton.setTitle(Localizations.RestorePurchase, for: .normal)
    }
    
    func setupWithModel(model: PageModel, price: String? = nil) {
        guard let model = model as? PurchasePageModel else { return }
        titleLabel.text = model.title
        pointLabels.enumerated().forEach { $0.element.text = model.bodyStrings.object(at: $0.offset) }
        
        if let price = price {
            if let inappDescription = model.inappDescription {
                priceDescriptionLabel.text = String(format: inappDescription, price)
            }
            trialDescriptionLabel.text = model.buttonDescription
        } else {
            priceDescriptionLabel.text = ""
            trialDescriptionLabel.text = ""
        }
        
        if let policy = model.policyUrl,
            let terms = model.termsUrl,
            let termsInfo = model.termsInfoText {
            let (fontName, fontSize, _) = self.infoTextAttributes
            let infoText = NSAttributedString.purchaseInfoText(from: termsInfo, policyUrl: policy, termsUrl: terms, fontName: fontName, fontSize: Float(fontSize), align: NSTextAlignment.justified, color: UIColor.white.withAlphaComponent(0.4))
            self.textView.attributedText = infoText
        }
        textView.scrollRangeToVisible(NSMakeRange(0, 0))
        backgroundImageView.image = UIImage(name: model.imageName ?? "OBPurchasePageBG1")
    }
}

extension OBPurchasePageView: OBPage {
    
    func setupWithModel(model: PageModel) {
        setupWithModel(model: model, price: nil)
    }
    
    func superViewDidLayoutSubviews() {
        addTitleGradint()
        calcTextViewHeight()
        restoreButton.layer.cornerRadius = self.restoreButton.frame.height / 2.0
    }
    
    func fadeInAnimationWithProgress(progress: CGFloat) {
        self.backgroundImageView.alpha = progress
        rightPartTrailingConstraint.constant = self.frame.width / 2 * (progress - 1)
    }
    
    func fadeOutAnimationWithProgress(progress: CGFloat) {
        backgroundImageView.alpha = (1 - progress * 2)
        rightPartTrailingConstraint.constant = self.frame.width / 2 * progress
    }
    
}

private extension OBPurchasePageView {
    
    func addTitleGradint() {
        let points = [GradientPoint(location: 0, color: UIColor(r: 255, g: 124, b: 5, a: 1.0)),
                      GradientPoint(location: 0.9, color: UIColor(r: 255, g: 174, b: 5, a: 1.0))]
        if let gradientImage = UIImage(size: titleLabel.frame.size, gradientPoints: points) {
            let color = UIColor(patternImage: gradientImage)
            titleLabel.textColor = color
        }
    }
    
    func calcTextViewHeight() {
        let numberOfVisibleRows = OnboardingUtils.isIphone() ? 3 : 5
        textViewHeight.constant = (textView.font?.pointSize ?? 0.0) * CGFloat(numberOfVisibleRows) * 1.2
    }
}
