//
//  GuitarViewTestViewController.swift
//  RealGuitarLite
//
//  Created by Alex on 4/24/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import UIKit

class OnboardingTestViewController: UIViewController {
    
    @IBOutlet weak var scrollView: PassThroughScrollView!
    
    var presenter: OnboardingPresenter?
    var delegate: OnboardingViewDelegate?
    var dataSource: OnboardingViewDataSource?
    var nextButton: (UIButton & OnboardingButton)!
    private var prevPage: Int = 0
    private var currentPage: Int = 0 {
        didSet {
            if oldValue != currentPage {
                prevPage = oldValue
                self.delegate?.currentPageChanged(to: currentPage, fromPageIndex: oldValue, slide: !self.scrollInProgress)
            }
        }
    }
    private var scrollInProgress = false
    private var pages = [UIView & OBPage]()
    private lazy var activityView: UIActivityIndicatorView = makeActivity()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildPages()
        setupNextButton()
        delegate?.viewDidLoad()
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutSlideScrollView(pages: pages)
        pages.forEach { $0.superViewDidLayoutSubviews() }
    }
}

private extension OnboardingTestViewController {
    
    func buildPages() {
        guard let count = dataSource?.countOfPages() else { return }
        pages = Array(0..<count).compactMap { pageView(at: $0) }
        pages.forEach { scrollView.addSubview($0) }
    }
    
    func pageView(at index: Int) -> (UIView & OBPage)? {
        guard let pageModel = dataSource?.onboardingPage(at: index) else { return nil }
        switch pageModel.type {
        case .normal:
            guard let normalPageModel = pageModel as? NormalPageModel else {return nil}
            let page = OBNormalPageView()
            page.setupWithModel(model: normalPageModel)
            return page
        case .purchase:
            guard let purchasePageModel = pageModel as? PurchasePageModel else {return nil}
            let page = OBPurchasePageView()
            page.delegate = self
            page.setupWithModel(model: purchasePageModel)
            return page
        default: return nil
        }
    }
    
    func scrollToNextPage() {
        guard currentPage < pages.count - 1 else { return }
        let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        let contentOffset = CGPoint.init(x: view.frame.width * CGFloat(currentPage + 1), y: 0)
        scrollView.setContentOffset(contentOffset, with: timingFunction, duration: 0.8)
        view.isUserInteractionEnabled = false
        scrollInProgress = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.scrollInProgress = false
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func setButtonTitle(pageIndex: Int) {
        nextButton.setTitle(
            dataSource?.nextButtonText(forPageAt: pageIndex),
            priceDescr: nil,
            showArrow: true,
            animated: false
        )
    }
    
    func layoutSlideScrollView(pages : [UIView]) {
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(pages.count), height: view.frame.height)
        print(scrollView.contentSize)
        scrollView.isPagingEnabled = true
        
        pages.enumerated().forEach { (index, page) in
            page.frame = CGRect(x: view.frame.width * CGFloat(index), y: 0, width: view.frame.width, height: view.frame.height)
        }
    }
    
    func setupNextButton() {
        nextButton = GutarButtonTest()
        view.addSubview(nextButton)
        nextButton.addTarget(self, action:#selector(self.onActionButtonClicked), for: .touchUpInside)
        nextButton.setupForView(view)
        setButtonTitle(pageIndex: 0)
        nextButton.accessibilityIdentifier = "nextButton"
    }
    
    @objc func onActionButtonClicked(_ sender: Any) {
        guard let pageModel = dataSource?.onboardingPage(at: currentPage) else { return }
        nextButton.didTouchUpInside(from: currentPage)
        switch pageModel.type {
        case .normal:
            scrollToNextPage()
        case .purchase:
            delegate?.purchaseButtonDidTap()
        case .pushNotification:
            scrollToNextPage()
        }
    }
    
    func makeActivity() -> UIActivityIndicatorView {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.hidesWhenStopped = true
        activity.frame = .init(x: 0, y: 0, width: 40, height: 40)
        activity.center = view.center
        view.addSubview(activity)
        return activity
    }
}

extension OnboardingTestViewController: OnboardingView {

    func showActivity(show: Bool) {
        if show {
            activityView.startAnimating()
            view.isUserInteractionEnabled = false
        } else {
            activityView.stopAnimating()
            view.isUserInteractionEnabled = true
        }
    }
    
    func updatePurchasePageData() {
        guard
            pages.count > 0,
            let pageModel = dataSource?.onboardingPage(at: pages.count - 1) as? PurchasePageModel,
            let page = pages.last as? OBPurchasePageView,
            let price = dataSource?.inappPrice()
            else { return }
        page.setupWithModel(model: pageModel, price: price)
        setButtonTitle(pageIndex: currentPage)
    }
    
    func close(completion: (() -> Void)?) {
        guard let navigationVC =  navigationController else {
            dismiss(animated: true, completion: completion)
            return
        }
        CATransaction.begin()
        navigationVC.popViewController(animated: true)
        CATransaction.setCompletionBlock(completion)
        CATransaction.commit()
    }
    
    func goToShopPage() {}
}

extension OnboardingTestViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y != 0 {
            scrollView.contentOffset.y = 0
        }
        let pageIndex = Int(scrollView.contentOffset.x/view.frame.width)
        let horizontalOffset: CGFloat = scrollView.contentOffset.x / scrollView.frame.width
        let progress = horizontalOffset - CGFloat(pageIndex)
        currentPage = pageIndex
        setButtonTitle(pageIndex: pageIndex)
        pages.object(at: pageIndex)?.fadeOutAnimationWithProgress(progress: progress)
        if let nextPage = pages.object(at: pageIndex + 1){
            nextPage.fadeInAnimationWithProgress(progress: progress)
        }
    }
}

extension OnboardingTestViewController: OBPurchasePageDelegate {
    func restoreAction() {
        delegate?.restoreAction()
    }
    
    func closeAction() {
        delegate?.closeAction()
    }
}

