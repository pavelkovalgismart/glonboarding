//
//  GutarButtonTest.swift
//  RealGuitarLite
//
//  Created by Alex on 5/13/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import UIKit
import Lottie

class GutarButtonTest: UIButton {
    
    private let soundManager = OnboardingSoundManager()
    private var lottieView = AnimationView(name: "butt")
    private var notes = AnimationView(name: "notes")
    let title = UILabel()
    let container = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let notesToButtonRatio: CGFloat = 672.0/552.0
        lottieView.frame = self.bounds
        title.frame = CGRect(x: lottieView.bounds.width * 0.15, y: lottieView.bounds.height * 0.25, width: lottieView.bounds.width * 0.65, height: lottieView.bounds.height * 0.5)
    
        notes.frame = CGRect(x: 0, y: 0, width: self.bounds.width * notesToButtonRatio, height: self.bounds.height * notesToButtonRatio)
        notes.center = self.center
    }
    
    //MARK: - Private
    private func customize() {
        addSubview(self.lottieView)
        lottieView.addSubview(title)
        lottieView.loopMode = .loop
        lottieView.contentMode = UIView.ContentMode.scaleAspectFit
        lottieView.isUserInteractionEnabled = false
        lottieView.play()
        
        notes.loopMode = .loop
        notes.contentMode = UIView.ContentMode.scaleAspectFit
        notes.play()
        notes.isUserInteractionEnabled = false

        title.textAlignment = .center
        title.textColor = .white
        title.font = UIFont.systemFont(ofSize: OnboardingUtils.isIphone() ? 16.0 : 21.0, weight: .bold)
        title.baselineAdjustment = .alignCenters
        title.numberOfLines = 2
        title.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.restartButtonAnimation), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func restartButtonAnimation() {
        lottieView.play()
        notes.play()
    }
}

extension GutarButtonTest: OnboardingButton {
    func startAnimation() {}
    
    func didTouchUpInside(from page: Int) {
        if #available(iOS 10.0, *) {
            let feedback = UISelectionFeedbackGenerator()
            feedback.selectionChanged()
        }
        soundManager.playSound(name: "onboarding_button_" + String(page), type: "mp3")
    }
    
    func setTitle(_ title: String?, priceDescr: String?, showArrow: Bool, animated: Bool) {
        self.title.text = title
    }
    
    func setTitle(_ title: String?, lastPage: Bool) {
        self.title.text = title
    }
    
    func setupForView(_ view: UIView) {
        container.isUserInteractionEnabled = false
        view.addSubview(notes)
        view.addSubview(container)
        
        view.bringSubviewToFront(container)
       
        // Default iPhone X
        var bottomConstant: CGFloat = -44.0
        var leadingConstant: CGFloat = 4.0
        let buttonAspecRatio: CGFloat = 552.0/186.0
        var buttonHeight: CGFloat = 100.0
        
        if OnboardingUtils.DeviceType.IS_IPHONE_5 {
            buttonHeight = 80.0
            bottomConstant = -32.0
            leadingConstant = -4.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_6 {
            bottomConstant = -40.0
            leadingConstant = 4.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_6P {
            leadingConstant = 4.0
        }
        
        if OnboardingUtils.DeviceType.IS_IPHONE_XR_OR_XS_MAX {
            leadingConstant = 4.0
        }
        
        if !OnboardingUtils.isIphone() {
            buttonHeight = 140.0
            bottomConstant = -100.0
            leadingConstant = 15.0
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: bottomConstant).isActive = true
        leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: leadingConstant).isActive = true
        widthAnchor.constraint(equalToConstant: buttonHeight * buttonAspecRatio).isActive = true
        heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
        
        container.translatesAutoresizingMaskIntoConstraints = false
        container.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        container.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        container.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0.0).isActive = true
    }
    
    func stopSound() {
         soundManager.player?.stop()
    }
}
