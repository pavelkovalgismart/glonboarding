//
//  HtmlController.swift
//  glmt
//
//  Created by dburchik on 8/29/18.
//  Copyright © 2018 gismart. All rights reserved.
//

import Foundation
import UIKit
import WebKit

private enum HtmlBrowserError: Error {
    case unableToInitializeBrowser
}

public enum HtmlControllerCallbackType {
    case showed
    case close
    case failed
    case action(name: String, params: [String : Any]?)
}

public final class OnboardingNewProtocolHtmlController: UIView {
    @IBOutlet private weak var fadeView: UIView!
    @IBOutlet private weak var progress: UIActivityIndicatorView!
    
    private var callback: ((HtmlControllerCallbackType) -> Void)?
    private var params: [String: Any]?
    private var paramsString: String!
    private var isHtmlOpaque: Bool?
    private var defaultZoom: CGFloat?
    private var browser: WKWebView?
    private var rootController: UIViewController?
    private let loadedGroup: DispatchGroup = DispatchGroup()
    
    func hide() {
        self.removeFromSuperview()
    }
    
    public static func prepareHtmlController(_ htmlPath: String, callback: @escaping (OnboardingNewProtocolHtmlController) -> Void) {
        DispatchQueue.main.async {

            let bundle = Bundle.main
            
            let topLevelObjects = bundle.loadNibNamed("OnboardingNewProtocolHtmlController", owner: OnboardingNewProtocolHtmlController.self, options: nil)
            guard let controller = topLevelObjects?.first as? OnboardingNewProtocolHtmlController else {
                fatalError("could not find HtmlController in bundle")
            }
            
            controller.loadHtml(URL(fileURLWithPath: htmlPath))
            callback(controller)
        }
    }
    
    public func present(_ controller: UIViewController, params: [String: Any], opaque: Bool, callback: @escaping (HtmlControllerCallbackType) -> Void) {
        self.callback = callback
        self.params = params
        self.isHtmlOpaque = opaque
        self.rootController = controller
        
        do {
            try self.addBrowser(params: self.params ?? [:], opaque: self.isHtmlOpaque ?? false)
            
            self.loadedGroup.notify(queue: DispatchQueue.main) {
                let changeParamsScriptString = String(describing: "_banner.setParams(\(self.paramsString!))")
                self.browser!.evaluateJavaScript(changeParamsScriptString, completionHandler: { result, error in
                    self.fadeInBrowser()
                })
            }
        } catch {
            callback(.failed)
            return
        }
        
        callback(.showed)
    }
    
    override public var alpha: CGFloat {
        didSet {
            self.fadeView?.alpha = alpha
            self.browser?.alpha = alpha
        }
    }
}

extension OnboardingNewProtocolHtmlController: WKScriptMessageHandler {
    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "banner_event" {
            guard let body = message.body as? [String : Any],
                let event = body["name"] as? String else {
                    return
            }
            
            if event == "close" {
                self.close()
            } else {
                let params = body["params"] as? [String : Any]
                self.action(event, params: params)
            }
        }
    }
}

extension OnboardingNewProtocolHtmlController: WKNavigationDelegate {
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            decisionHandler(.cancel)
            if let url = navigationAction.request.url {
                
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            decisionHandler(.allow)
        }
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadedGroup.leave()
    }
}

extension OnboardingNewProtocolHtmlController: UIScrollViewDelegate {
    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if defaultZoom == 0.0 {
            defaultZoom = scrollView.zoomScale
        } else {
            scrollView.zoomScale = defaultZoom ?? 0.0
        }
    }
}

private extension OnboardingNewProtocolHtmlController {
    func loadHtml(_ url: URL) {
        self.loadedGroup.enter()

        let disableSelectionScriptString = "document.documentElement.style.webkitUserSelect='none';"
        let disableCalloutScriptString = "document.documentElement.style.webkitTouchCallout='none';"
        
        let disableSelectionScript = WKUserScript(source: disableSelectionScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let disableCalloutScript = WKUserScript(source: disableCalloutScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let webConfig = WKWebViewConfiguration()
        let userController = WKUserContentController()
        
        userController.addUserScript(disableSelectionScript)
        userController.addUserScript(disableCalloutScript)
        userController.add(self, name: "banner_event")
        
        webConfig.userContentController = userController
        
        self.browser = WKWebView(frame: self.frame, configuration: webConfig)
        self.browser?.navigationDelegate = self
        self.browser?.loadFileURL(url, allowingReadAccessTo: url)
    }
    
    func addBrowser(params: [String : Any], opaque: Bool) throws {
        guard let browser = self.browser,
            let controller = self.rootController,
            let jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
            let paramsString = String(data: jsonData, encoding: .utf8) else {
                throw HtmlBrowserError.unableToInitializeBrowser
        }
        
        self.paramsString = paramsString
        
        DispatchQueue.main.async {
            let rect = controller.view.bounds
            self.frame = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height)
            
            browser.navigationDelegate = self
            browser.scrollView.isScrollEnabled = false
            browser.scrollView.maximumZoomScale = 1.0
            browser.scrollView.minimumZoomScale = 1.0
            
            browser.scrollView.delegate = self
            self.fadeView.addSubview(browser)
            
            if opaque {
                browser.backgroundColor = .clear
                browser.scrollView.backgroundColor = .clear
                self.fadeView.backgroundColor = .clear
                self.backgroundColor = .clear
                browser.isOpaque = false
            }
            
            self.setupBrowserConstraints()
            
            self.fadeOutBrowser()
            controller.view.addSubview(self)
        }
    }
    
    func fadeOutBrowser() {
        self.alpha = 0.0
    }
    
    private func fadeInBrowser() {
        UIView.animate(withDuration: 0.0, delay: 0.2, options: .layoutSubviews, animations: {
            self.alpha = 1.0
        }, completion: nil)
    }
    
    func close() {
        self.callback?(.close)
        self.hide()
    }
    
    func action(_ action: String, params: [String : Any]?) {
        self.callback?(.action(name: action, params: params))
        self.hide()
    }
    
    func setupBrowserConstraints() {
        guard let browser = self.browser else {
            return
        }
        
        let paddingConstant: CGFloat = 0.0
        
        browser.translatesAutoresizingMaskIntoConstraints = false
        let iPhoneX = UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.size.height == 2436
        
        if iPhoneX {
            browser.topAnchor.constraint(equalTo: self.fadeView.topAnchor, constant: paddingConstant).isActive = true
            browser.bottomAnchor.constraint(equalTo: self.fadeView.bottomAnchor, constant: 21.0).isActive = true
            browser.leadingAnchor.constraint(equalTo: self.fadeView.leadingAnchor, constant: 0).isActive = true
            browser.trailingAnchor.constraint(equalTo: self.fadeView.trailingAnchor, constant: 0).isActive = true
        } else {
            browser.topAnchor.constraint(equalTo: self.fadeView.topAnchor, constant: paddingConstant).isActive = true
            browser.bottomAnchor.constraint(equalTo: self.fadeView.bottomAnchor, constant: -paddingConstant).isActive = true
            browser.leadingAnchor.constraint(equalTo: self.fadeView.leadingAnchor, constant: paddingConstant).isActive = true
            browser.trailingAnchor.constraint(equalTo: self.fadeView.trailingAnchor, constant: -paddingConstant).isActive = true
        }
    }
}

