//
//  GLCache.h
//  Pianino
//
//  Created by Dzmitry Burchyk on 10/27/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBCache : NSObject

- (void)cacheData:(NSData *)data forKey:(NSString *)key;
- (NSData *)cachedDataForKey:(NSString *)key;
- (NSURL *)cachedDataUrl:(NSString *)key;

@end
