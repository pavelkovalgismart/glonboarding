//
//  GLCache.m
//  Pianino
//
//  Created by Dzmitry Burchyk on 10/27/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import "OBCache.h"

@implementation OBCache

- (NSString *)prepareKey:(NSString *)key {
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *extension = [key pathExtension];
    NSString *withoutExt = [key stringByDeletingPathExtension];
    
    NSString *prepared = [[[withoutExt componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""] stringByAppendingPathExtension:extension];
    
    return prepared;
}

- (void)cacheData:(NSData *)data forKey:(NSString *)key {
    NSString *path = [[self cacheFolder] stringByAppendingPathComponent:[self prepareKey:key]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        NSAssert(!error, @"Error while removing previous cache for the key");
        if (error) {
            return;
        }
    }
    
    [data writeToFile:path atomically:YES];
}

- (NSData *)cachedDataForKey:(NSString *)key {
    NSString *path = [[self cacheFolder] stringByAppendingPathComponent:[self prepareKey:key]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error;
        NSData *data = [NSData dataWithContentsOfFile:path options:0 error:&error];
        NSAssert(!error, @"Error while reading the existing file from cache");
        if (!error && data) {
            return data;
        }
    }
    
    return nil;
}

- (NSURL *)cachedDataUrl:(NSString *)key {
    NSString *path = [[self cacheFolder] stringByAppendingPathComponent:[self prepareKey:key]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return [NSURL fileURLWithPath:path];
    }
    
    return nil;
}

- (NSString *)cacheFolder
{
    NSArray *pathes = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [pathes firstObject];
    NSAssert(cachePath, @"Could not find a cache path");
    
    return cachePath;
}

@end
