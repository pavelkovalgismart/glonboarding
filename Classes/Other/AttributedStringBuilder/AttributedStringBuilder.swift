//
//  AttributedStringBuilder.swift
//  RealDrumFree
//
//  Created by Nikolay Tsygankov on 07/05/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class AttributedStringBuilder {
    static let defaultFontSize: CGFloat = 17
    private enum Tags {
        static let bold = TagToProcess(regExp: "<b>[\\s\\S]*?</b>", attributes: [boldAttributes], openLength: 3, closedLength: 4)
        static let italic = TagToProcess(regExp: "<em>[\\s\\S]*?</em>", attributes: [italicAttributes], openLength: 4, closedLength: 5)
        static let underline = TagToProcess(regExp: "<span class=\"u\">(.*?)</span>", attributes: [underlineAttributes], openLength: 5, closedLength: 7)
        static let strike = TagToProcess(regExp: "<span class=\"s\">(.*?)</span>", attributes: [strikeAttribures], openLength: 7, closedLength: 5)
    }
    
    static let boldFont = UIFont.boldSystemFont(ofSize: defaultFontSize)
    static let boldAttributes = [NSAttributedString.Key.font : boldFont]
    static let italicFont = UIFont.italicSystemFont(ofSize: defaultFontSize)
    static let italicAttributes = [NSAttributedString.Key.font : italicFont]
    static let underlineAttributes = [NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    static let strikeAttribures = [NSAttributedString.Key.strikethroughStyle : NSUnderlineStyle.single.rawValue]
    
    struct TagToProcess {
        let regExp: String
        let attributes: [[NSAttributedString.Key : Any]]
        let openLength: Int
        let closedLength: Int
    }
    
    private let tagsToProcess = [Tags.bold]
    
    func attributedString(stringWithTags: String) -> NSAttributedString? {
        let attrStr = NSMutableAttributedString(string: stringWithTags)
        for tag in self.tagsToProcess {
            var rangeOfSearchText = self.findTextWithTag(regExp: tag.regExp, str: attrStr.string)
            while rangeOfSearchText.length != 0 {
                let rangeWithoutTags = self.removeTagsFromString(str: attrStr.mutableString, range: &rangeOfSearchText, open: tag.openLength, closed: tag.closedLength)
                for attributes in tag.attributes {
                    attrStr.addAttributes(attributes, range: rangeWithoutTags)
                }
                rangeOfSearchText = self.findTextWithTag(regExp: tag.regExp, str: attrStr.mutableString as String)
            }
        }
        return attrStr
    }
    
    // MARK: Private
    private func findTextWithTag(regExp: String, str: String) -> NSRange {
        guard let regex = try? NSRegularExpression(pattern: regExp, options: .caseInsensitive) else { return NSRange(location: 0, length: 0) }
        let result =  regex.firstMatch(in: str, options: .reportProgress, range: NSRange(location: 0, length: str.count))
        if let result = result {
            return result.range
        } else {
            return NSRange(location: 0, length: 0)
        }
    }
    
    private func removeTagsFromString(str: NSMutableString, range: inout NSRange, open: Int, closed: Int) -> NSRange {
        let openTagRange = NSRange(location: range.location, length: open)
        str.deleteCharacters(in: openTagRange)
        range.length -= open
        
        let closedTagRange = NSRange(location: range.location + range.length - closed, length: closed)
        str.deleteCharacters(in: closedTagRange)
        range.length -= closed
        return range
    }
}
