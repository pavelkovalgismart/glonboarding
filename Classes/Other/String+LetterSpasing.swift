//
//  String+LetterSpasing.swift
//  RealGuitarLite
//
//  Created by Alex on 2/27/19.
//  Copyright © 2019 Alex Parhimovich. All rights reserved.
//

import Foundation

extension String {
    func attributedString(withLetterSpasing value: Double) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttribute(NSAttributedStringKey.kern, value: CGFloat(-0.4), range: NSRange(location: 0, length: attributedString.length))
        return attributedString
    }
}
