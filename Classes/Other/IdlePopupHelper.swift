//
//  IdlePopupHelper.swift
//  RealGuitarLite
//
//  Created by Pavel Koval on 12/12/18.
//  Copyright © 2018 Alex Parhimovich. All rights reserved.
//

import Foundation

protocol IdlePopupHelperDelegate: AnyObject {
    func showIdlePopup()
}

class IdlePopupHelper {
    weak var delegate: IdlePopupHelperDelegate?
    let timeout: TimeInterval
    
    private var idleTimer: Timer?
    
    init(timeout: Int = 10) {
        self.timeout = TimeInterval(timeout)
    }
    
    func startIdleTimer() {
        self.idleTimer = self.makeTimer()
    }
    
    func stopTimer() {
        self.idleTimer?.invalidate()
    }
    
    //MARK: Private
    private func makeTimer() -> Timer {
        let timer = Timer.scheduledTimer(timeInterval: self.timeout, target: self, selector: #selector(self.onTimerFire), userInfo: nil, repeats: false)
        return timer
    }
    
    @objc private func onTimerFire() {
        self.delegate?.showIdlePopup()
    }
}
