//
//  OnboardingSoundManager.swift
//  RealGuitar
//
//  Created by Alex on 5/21/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

class OnboardingSoundManager {
    
    var player: AVAudioPlayer?
    
    func playSound(name: String, type: String) {
        guard let path = Bundle.main.path(forResource: name, ofType: type) else { return }
        let url = URL(fileURLWithPath: path)
        self.player = try? AVAudioPlayer(contentsOf: url)
        player?.play()
    }
    
    func preloadSound(withName name: String, type: String) -> SystemSoundID? {
        guard let url = Bundle.main.url(forResource: name, withExtension: type) else { return nil }
        var mySound = SystemSoundID()
        AudioServicesCreateSystemSoundID(url as CFURL, &mySound)
        return mySound
    }
    
    func playPreloadedSound(withID soundId: SystemSoundID) {
        AudioServicesPlaySystemSoundWithCompletion(soundId, nil)
    }
    
    func disposeLoadedSound(withId soundId: SystemSoundID) {
        AudioServicesDisposeSystemSoundID(soundId)
    }
}
