//
//  InAppPurchaseManager.swift
//  RealGuitar
//
//  Created by Alex on 5/21/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation
import StoreKit

protocol OnboardingPurchaseManager {
    func isBundlePurchased() -> Bool
    func product(with inappId: String, completion:@escaping (OnboardingIAProductInfo?, Error?) -> Void)
    func onboarding_addPayment(_ inappId: String,
                               additionalParams: [String: String],
                               success: @escaping () -> (), failure: @escaping (Error?) -> ())
    func onboarding_restore(success: @escaping () -> (),
                            failure: @escaping (Error?) -> ())
    var purchaseItemsUpdateKey: NSNotification.Name? { get }
    var promotedIapClicked: NSNotification.Name? { get }
}

protocol OnboardingIAProductInfo {
    var localizedPrice: String? { get }
}
