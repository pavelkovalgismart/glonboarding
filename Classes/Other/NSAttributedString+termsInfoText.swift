//
//  NSAttributedString+termsInfoText.swift
//  RealDrumFree
//
//  Created by Nikolay Tsygankov on 10/05/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

extension NSAttributedString {
    class func purchaseInfoText(from termsInfoText: String,
                                policyUrl: String,
                                termsUrl: String,
                                fontName: String,
                                fontSize: Float,
                                align: NSTextAlignment,
                                color: UIColor? = nil) -> NSAttributedString? {
    let countOfParams = termsInfoText.components(separatedBy: "%@").count - 1
    var arguments: [CVarArg] = [policyUrl, termsUrl]
    if countOfParams > 2 {
        arguments.insert("", at: 0)
    }
    guard let textData = "<style>body{font-family:'\(fontName)'; font-size:\(fontSize)px; color:'rgb(65,66,88)';}</style>\(String(format: termsInfoText, arguments: arguments))".data(using: .unicode) else { return nil }

        guard let infoText = try? NSMutableAttributedString(data: textData, options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html] , documentAttributes: nil) else { return nil }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = align
        infoText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, infoText.length))
        if let textColor = color {
            infoText.addAttribute(NSAttributedString.Key.foregroundColor, value: textColor, range: NSMakeRange(0, infoText.length))
        }
        return infoText
    }
}
