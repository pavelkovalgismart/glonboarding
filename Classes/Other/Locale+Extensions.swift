//
//  Locale+Extensions.swift
//  RealGuitar
//
//  Created by Alex Lednik on 9/12/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

extension Locale {
    static var currentLanguage: String {
        guard let localeIdentifier = Locale.preferredLanguages.first else {
            return "en"
        }
        return Locale.components(fromIdentifier: localeIdentifier)["kCFLocaleLanguageCodeKey"] ?? "en"
    }
}
