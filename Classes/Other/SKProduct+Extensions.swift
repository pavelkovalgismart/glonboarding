//
//  SKProduct+Extensions.swift
//  RealGuitarLite
//
//  Created by Alex Lednik on 9/20/18.
//  Copyright © 2018 Alex Parhimovich. All rights reserved.
//

import Foundation
import StoreKit

extension SKProduct {
    func localizedPrice() -> String? {
        guard self.productIdentifier.count > 0 else { return nil }
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.priceLocale
        return formatter.string(from: self.price)
    }
    
}
