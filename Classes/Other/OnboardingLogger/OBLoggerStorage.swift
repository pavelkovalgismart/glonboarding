//
//  LoggerStoraga.swift
//  RealTuner
//
//  Created by Alex Parkhimovich on 4/26/17.
//  Copyright © 2017 GiSmart. All rights reserved.
//

import Foundation

class OBLoggerStorage: NSObject {

    static func countForKey(key:String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }
    
    static func incValue(event:String) {
        let incCount = OBLoggerStorage.countForKey(key: event) + 1
        UserDefaults.standard.setValue(incCount, forKey: event)
    }
    
    
}

