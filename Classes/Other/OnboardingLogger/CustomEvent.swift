//
//  TunerEvent.swift
//  RealTuner
//
//  Created by Alex Parkhimovich on 5/4/17.
//  Copyright © 2017 GiSmart. All rights reserved.
//

import Foundation
import GLLogger

class CustomEvent: OBEventLog {
    let name: String
    let timed: Bool = true
    var param: [String: Any]?
    let startInterval = Date().timeIntervalSince1970
    
    init(name : String) {
        self.name = name
    }
    
    func endTimed() {
        let time = Date().timeIntervalSince1970 - startInterval
        param?["time"] = round(time)
    }
}
