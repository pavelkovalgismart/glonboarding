//
//  TimedEvent.swift
//  RealTuner
//
//  Created by Alex Parkhimovich on 5/3/17.
//  Copyright © 2017 GiSmart. All rights reserved.
//

import Foundation
import GLLogger

protocol OBEventLog {
    var name: String { get }
    var param: [String: Any]? { get }
    var timed: Bool { get }

    func endTimed()
}
