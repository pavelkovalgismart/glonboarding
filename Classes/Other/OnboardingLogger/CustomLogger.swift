
//
//  TunerLogger.swift
//  RealTuner
//
//  Created by Alex Parkhimovich on 4/20/17.
//  Copyright © 2017 GiSmart. All rights reserved.
//

import UIKit
import GLLogger
import glboardinglogger

enum CustomEvents {
    static let continueWithLimitedVersion = "onboarding_continue_with_limited_version"
    static let purchaseCompleted = "purchase_completed"
    static let onboardingStartTrialPressed = "onboarding_start_trial_pressed"
    static let onboardingClosePressed = "onboarding_close_pressed"
    static let onboardingRestore = "onboarding_restore"
    static let askAgainAfterContinueFreeClosed = "ask_again_trial_notification"
    static let askAgainAfterCancelPaymentClosed = "limited_offer_trial_notification"
}
