//
//  AskAgainNotificationViewController.swift
//  RealDrumFree
//
//  Created by NIkolay Tsygankov on 5/2/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import UIKit

class AskAgainNotificationViewController: UIViewController {
    
    var okHandler: (() -> Void)?
    var cancelHandler: (() -> Void)?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    private var viewData: AskAgainModel!
    
    private enum UI {
        static let showAnimationDuration: TimeInterval = 0.6
        static let dissapearDuration: TimeInterval = 0.5
    }
    
    //MARK: Public
    class func create() -> AskAgainNotificationViewController {
        let popup = AskAgainNotificationViewController(nibName: "AskAgainNotificationViewController", bundle: Bundle.main)
        popup.modalTransitionStyle = .crossDissolve
        popup.modalPresentationStyle = .overCurrentContext
        return popup
    }
    
    func show(from vc: UIViewController, viewData: AskAgainModel) {
        self.viewData = viewData
        
        vc.present(self, animated: false, completion: self.showContent)
    }
    
    func dismissSelf(completion: @escaping (() -> Void) ) {
        UIView.animate(withDuration: UI.dissapearDuration) {
            self.view.alpha = 0
        }
        UIView.animate(withDuration: UI.dissapearDuration * 0.3, animations: {
            self.contentView.transform = .init(scaleX: 1.1, y: 1.1)
        }) { (completed) in
            UIView.animate(withDuration: UI.dissapearDuration * 0.7, animations: {
                self.contentView.transform = .init(scaleX: 0.5, y: 0.5)
                self.contentView.alpha = 0
            }, completion: { (completed) in
                self.dismiss(animated: false, completion: completion)
            })
        }
    }

    //MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupViewData()
    }
    
    //MARK: Private
    private func setupViewData() {
        if self.viewData.body != nil {
            self.titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            self.titleLabel.text = self.viewData.title
            self.bodyLabel.attributedText = self.viewData.attributedBody
        } else {
            self.bodyLabel.attributedText = nil
            self.titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            if let attrTitle = self.viewData.attributedTitle {
                self.titleLabel.attributedText = attrTitle
            } else {
                self.titleLabel.text = self.viewData.title
            }
        }
        
        self.okButton.setTitle(self.viewData.okButton, for: .normal)
        if let cancelTitle = self.viewData.cancelButton {
            self.cancelButton.isHidden = false
            self.cancelButton.setTitle(cancelTitle, for: .normal)
        } else {
            self.cancelButton.isHidden = true
        }
    }
    
    private func setupUI() {
        self.contentView.layer.cornerRadius = 16
        self.contentView.clipsToBounds = true
        self.contentView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        self.contentView.layer.shadowOffset = .init(width: 0, height: 2)
        self.contentView.layer.shadowRadius = 8
        self.contentView.layer.shadowOpacity = 1
        
        self.okButton.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.okButton.layer.shadowRadius = 6
        self.okButton.layer.shadowOffset = .init(width: 0, height: 2)
        self.okButton.layer.shadowOpacity = 1
    }
    
    private func showContent() {
        UIView.animate(withDuration: UI.showAnimationDuration * 0.5) {
            self.view.alpha = 1
        }
        
        let scaleFrom: CGFloat =  0.6
        let scaleTo: CGFloat = 1
        self.contentView.transform = .init(scaleX: scaleFrom, y: scaleFrom)
        UIView.animate(withDuration: UI.showAnimationDuration, delay: 0, usingSpringWithDamping: 0.35, initialSpringVelocity: 8, options: .curveLinear, animations: {
            self.contentView.transform = .init(scaleX: scaleTo, y: scaleTo)
            self.contentView.alpha = 1.0
        }, completion: nil)
        
    }
    
    //MARK: Actions
    @IBAction func onCancelClicked(_ sender: Any) {
        self.cancelHandler?()
    }
    
    @IBAction func onOkClicked(_ sender: Any) {
        self.okHandler?()
    }
}
