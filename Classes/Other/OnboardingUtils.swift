//
//  OnboardingUtils.swift
//  RealGuitar
//
//  Created by Alex on 5/21/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import UIKit

enum OnboardingUtils {
    static func isIphone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    static func isSerieXDevice() -> Bool {
        return DeviceType.IS_IPHONE_X_OR_XS || DeviceType.IS_IPHONE_XR_OR_XS_MAX
    }
    
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS    = isIphone() && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5            = isIphone() && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6            = isIphone() && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P           = isIphone() && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X_OR_XS      = isIphone() && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XR_OR_XS_MAX = isIphone() && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        
        private static func isIphone() -> Bool {
            return OnboardingUtils.isIphone()
        }
    }
}
