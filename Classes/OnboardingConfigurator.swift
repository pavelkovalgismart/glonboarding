//
//  OnboardingConfigurator.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation
import glprobabilitypurchaseanalyzer
import glanalytics

class OnboadingConfigurator: NSObject {
    class func configure(configSettings: OnboardingSettings,
                         moduleDelegate: OnboardingPresenterDelegate,
                         inAppPurchaseManager: OnboardingPurchaseManager,
                         loger: AnalyticEventsLogging?,
                         view: OnboardingView & UIViewController,
                         probabilityAnalyzer: GLProbabilityPurchaseAnalyzer) -> UIViewController? {
        let logger = loger
        let dataService = OnboardingDataService(settings: configSettings)
        let presenter = OnboardingPresenter(dataService: dataService,
                                            logger: logger,
                                            inAppPurchaseManager: inAppPurchaseManager,
                                            view: view)
        presenter.delegate = moduleDelegate
        guard presenter.canBeShowed() else { return nil }

        view.presenter = presenter
        view.delegate = presenter
        view.dataSource = presenter
        return view
    }
}
