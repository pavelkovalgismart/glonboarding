//
//  AskAgainNotificationModel.swift
//  RealDrumFree
//
//  Created by NIkolay Tsygankov on 5/2/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

struct AskAgainModel {
    let enabled: Bool
    let title: String
    let body: String?
    var attributedBody: NSAttributedString? {
        get {
            guard let body = self.body else { return nil }
            return AttributedStringBuilder().attributedString(stringWithTags: body)
        }
    }
    var attributedTitle: NSAttributedString? {
        get {
            return AttributedStringBuilder().attributedString(stringWithTags: self.title)
        }
    }
    let okButton: String
    let cancelButton: String?
    let replaceInappId: String?
    let replaceTitleText: String?
    let impressionLimit: Int
    let impressionIndex: Int
    
    let htmlEnabled: Bool
    let htmlPageUrl: URL?
    let freeDaysCount: Int?
}

extension AskAgainModel {
    private enum Keys {
        static let enabled = "enabled"
        static let htmlEnabled = "html_enabled"
        static let title = "title"
        static let okButton = "button_ok"
        static let cancelButton = "button_cancel"
        static let replaceInappId = "replace_innap_id"
        static let body = "body"
        static let impressionLimit = "impression_limit"
        static let replaceTitleText = "replace_title_text"
        static let impressionIndex = "impression_index"
        static let htmlPageUrl = "html_page_url"
        static let freeDaysCount = "free_days_count"
    }
    
    init?(jsonData: [AnyHashable : Any]) {
        guard let title = jsonData[Keys.title] as? String,
            let okButton = jsonData[Keys.okButton] as? String,
            let enabled = jsonData[Keys.enabled] as? Bool else { return nil }
        self.title = title
        self.okButton = okButton
        self.enabled = enabled
        self.cancelButton = jsonData[Keys.cancelButton] as? String
        self.replaceInappId = jsonData[Keys.replaceInappId] as? String
        self.body = jsonData[Keys.body] as? String
        self.replaceTitleText = jsonData[Keys.replaceTitleText] as? String
        self.impressionLimit = (jsonData[Keys.impressionLimit] as? Int ) ?? 1
        self.impressionIndex = (jsonData[Keys.impressionIndex] as? Int ) ?? 1
        self.htmlEnabled = (jsonData[Keys.htmlEnabled] as? Bool ) ?? false
        
        if self.htmlEnabled {
            guard let htmlPageUrlString = jsonData[Keys.htmlPageUrl] as? String,
                let url = URL(string: htmlPageUrlString) else { return nil }
            self.htmlPageUrl = url
            self.freeDaysCount = jsonData[Keys.freeDaysCount] as? Int ?? 7
        } else {
            self.htmlPageUrl = nil
            self.freeDaysCount = nil
        }
    }
}

