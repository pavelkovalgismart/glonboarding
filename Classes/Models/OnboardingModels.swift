//
//  OnboardingModels.swift
//  Drums
//
//  Created by Nikolay Tsygankov on 03/03/2018.
//  Copyright © 2018 Gismart. All rights reserved.
//

import Foundation

enum PageType {
    case normal
    case purchase
    case pushNotification
}

enum OnboardingCrossType: Int {
    case old = 0
    case new = 1
}

protocol PageModel {
    var type: PageType { get }
    var buttonText: String? { get set }
}

struct NormalPageModel: PageModel {
    let type: PageType = .normal
    var buttonText: String?
    let title: String?
    let body: String?
    let image: String?
}

struct PurchasePageModel: PageModel {
    let type: PageType = .purchase
    var buttonText: String?
    var title: String?
    var subtitle: String?
    let bodyStrings: [String]
    let imageName: String?
    let continueFreeButtonText: String?
    var inappDescription: String?
    let termsInfoText: String?
    let policyUrl: String?
    let termsUrl: String?
    let trialDurationText: String?
    let buttonPriceDescription: String?
    let buttonDescription: String?
    let restoreTitle: String?
    let crossType: OnboardingCrossType?
}

struct PushNotificationPageModel: PageModel {
    let type: PageType = .pushNotification
    var buttonText: String?
    let title: String?
    let body: String?
    let image: String?
    let pushDescription: String?
    let allowButtonTitle: String?
}

struct PageModelFactory {
    private enum Keys {
        static let title = "title"
        static let subtitle = "subtitle"
        static let body = "body"
        static let image = "image"
        static let buttonText = "button_text"
        static let bodyStrings = "body_strings"
        static let continueFreeButton = "continue_button_text"
        static let inappDescription = "inapp_description"
        static let termsInfoText = "terms_info_text"
        static let policyUrlValue = "policy_url"
        static let termsUrlValue = "terms_url"
        static let pushDescription = "push_description"
        static let allowButtonTitle = "allow_button"
        static let trialDurationText = "trial_duration_text"
        static let buttonPriceDescription = "btn_price_descr"
        static let buttonDescription = "btn_descr"
        static let restoreTitle = "restore_button"
        static let crossType = "cross_type"
    }

    static func makePageModel(with dict: [AnyHashable : Any]) -> PageModel? {
        let title = dict[Keys.title] as? String
        let subtitle = dict[Keys.subtitle] as? String
        let body = dict[Keys.body] as? String
        let buttonText = dict[Keys.buttonText] as? String
        let imageName = dict[Keys.image] as? String

        if
            let bodyStrings = dict[Keys.bodyStrings] as? [String],
            let continueFreeButton = dict[Keys.continueFreeButton] as? String
        {
                let inappDescription = dict[Keys.inappDescription] as? String
                let policyUrl = dict[Keys.policyUrlValue] as? String
                let termsUrl = dict[Keys.termsUrlValue] as? String
                let termsInfo = dict[Keys.termsInfoText] as? String
                let trialDuration = dict[Keys.trialDurationText] as? String
                let btnPriceDescription = dict[Keys.buttonPriceDescription] as? String
                let btnDescription = dict[Keys.buttonDescription] as? String
                let restoreTitle = dict[Keys.restoreTitle] as? String
                var crossType: OnboardingCrossType? = nil
                if let crossIntValue = dict[Keys.crossType] as? Int {
                    crossType = OnboardingCrossType(rawValue: crossIntValue)
                }
                return PurchasePageModel(buttonText: buttonText,
                                         title: title,
                                         subtitle: subtitle,
                                         bodyStrings: bodyStrings,
                                         imageName: imageName,
                                         continueFreeButtonText: continueFreeButton,
                                         inappDescription: inappDescription,
                                         termsInfoText: termsInfo,
                                         policyUrl: policyUrl,
                                         termsUrl: termsUrl,
                                         trialDurationText: trialDuration,
                                         buttonPriceDescription: btnPriceDescription,
                                         buttonDescription: btnDescription,
                                         restoreTitle: restoreTitle,
                                         crossType: crossType)
        } else if let pushDescription = dict[Keys.pushDescription] as? String,
            let allowButtonTitle = dict[Keys.allowButtonTitle] as? String {
                return PushNotificationPageModel(buttonText: buttonText,
                                                 title: title,
                                                 body: body,
                                                 image: imageName,
                                                 pushDescription: pushDescription,
                                                 allowButtonTitle: allowButtonTitle)
        } else {
            return NormalPageModel(buttonText: buttonText,
                                   title: title,
                                   body: body,
                                   image: imageName)
        }
    }
}
