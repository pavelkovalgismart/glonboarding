//
//  HtmlAskAgainModel.swift
//  RealGuitarLite
//
//  Created by Alex on 10/15/18.
//  Copyright © 2018 Alex Parhimovich. All rights reserved.
//

import Foundation

struct HtmlAskAgainModel {
    let htmlPageUrl: URL
    let price: String?
    let replaceInappId: String?
    let replaceTitle: String?
    let replaceSubtitle: String?
    let replaceInappDescription: String?
    let replaceButtonText: String?
}

extension HtmlAskAgainModel {
    private enum Keys {
        static let htmlPageUrl = "html_page_url"
        static let price = "price"
        static let replaceInappId = "replace_inapp_id"
        static let replaceTitle = "replace_title"
        static let replaceSubtitle = "replace_subtitle"
        static let replaceInappDescription = "inapp_description"
        static let replaceButtonText = "replace_button_text"
    }
    
    init?(jsonData: [AnyHashable : Any]) {
        guard let htmlPageUrlString = jsonData[Keys.htmlPageUrl] as? String,
            let url = URL(string: htmlPageUrlString)
            else { return nil }
        self.htmlPageUrl = url
        self.price = jsonData[Keys.price] as? String
        self.replaceInappId = jsonData[Keys.replaceInappId] as? String
        self.replaceTitle = jsonData[Keys.replaceTitle] as? String
        self.replaceSubtitle = jsonData[Keys.replaceSubtitle] as? String
        self.replaceInappDescription = jsonData[Keys.replaceInappDescription] as? String
        self.replaceButtonText = jsonData[Keys.replaceButtonText] as? String
    }
}
