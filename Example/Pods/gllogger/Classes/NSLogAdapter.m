//
//  NSLogAdapter.m
//  Metronome
//
//  Created by Dzmitry Burchyk on 3/23/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import "NSLogAdapter.h"

@interface NSLogAdapter()

@property(nonatomic) id<ITimeEventContainer> container;

@end

@implementation NSLogAdapter

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container {
    self = [super init];
    if (self) {
        _container = container;
    }
    return self;
}

- (void)logEvent:(NSString *)eventName {
    [self logEvent:eventName withParameters:nil];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    NSLog(@"LOG EVENT: %@\r\nPARAMS: %@", eventName, parameters);
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    [self logEvent:eventName withParameters:nil timed:timed];
 }

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [self logEvent:eventName withParameters:parameters];
    if (timed) {
        [self.container storeStartTimeForEventName:eventName];
    }
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    __weak typeof(self) wself = self;
    NSLog(@"LOG END TIMED EVENT: %@\r\nPARAMS: %@", eventName, parameters);
    [self.container getUpdatedParametersForEventName:eventName withParameters:parameters withNewEvent:^(NSString *updatedName, NSDictionary *updatedParameters, NSError *error) {
        if (!error) {
            NSLog(@"LOG UPDATED END TIMED EVENT: %@\r\nPARAMS: %@", updatedName, updatedParameters);
            [wself logEvent:updatedName withParameters:updatedParameters];
        }
    }];
}

@end
