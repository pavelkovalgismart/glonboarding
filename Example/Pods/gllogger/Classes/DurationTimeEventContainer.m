//
//  TimeLoggerContainer.m
//  Pianino
//
//  Created by Kiryl Belasheuski on 2/28/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

#import "DurationTimeEventContainer.h"
#import "GLLogger.h"

static NSString *const errorDomain = @"com.gismart.glloger.DurationTimeEventContainer";
static char *const queueID = "com.gismart.timeloggercontainer";

@interface DurationTimeEventContainer()

@property (atomic, strong) NSMutableDictionary<NSString *, NSNumber *> *timestamps;
@property (atomic, strong) NSNumberFormatter *durationFormat;
@property dispatch_queue_t dispatchAccessQueue;

@end

@implementation DurationTimeEventContainer

- (instancetype)init {
    self = [super init];
    if (self) {
        _timestamps = [NSMutableDictionary new];
        _dispatchAccessQueue = DurationTimeEventContainer.sharedAccessQueue;
        _durationFormat = [NSNumberFormatter new];
        [_durationFormat setNumberStyle:NSNumberFormatterDecimalStyle];
        [_durationFormat setMaximumFractionDigits:2];
        [_durationFormat setRoundingMode:NSNumberFormatterRoundUp];
    }
    return self;
}

+ (dispatch_queue_t)sharedAccessQueue {
    static dispatch_queue_t sharedQueue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedQueue = dispatch_queue_create(queueID, NULL);
    });
    return sharedQueue;
}

- (void)storeStartTimeForEventName:(NSString *)eventName {
    dispatch_async(_dispatchAccessQueue, ^{
        _timestamps[eventName] = @([NSDate timeIntervalSinceReferenceDate]);
    });
}

- (void)getUpdatedParametersForEventName:(NSString *)eventName
                          withParameters:(NSDictionary *)parameters
                            withNewEvent:(void (^)(NSString * _Nullable updatedName, NSDictionary* _Nullable updatedParameters, NSError *error))block {
    
    dispatch_async(_dispatchAccessQueue, ^{
        NSTimeInterval timeInterval = [self getDurationForEventName:eventName];
        NSMutableDictionary *mutableParameters;
        if (parameters) {
            mutableParameters = [parameters mutableCopy];
        } else {
            mutableParameters = [NSMutableDictionary new];
        }

        if (timeInterval != 0) {
            NSString *duration = [_durationFormat stringFromNumber:@(timeInterval)];
            [mutableParameters setValue:duration forKey:kDurationTimeKey];
            block([NSString stringWithFormat:@"%@_end", eventName], [mutableParameters copy], nil);
        } else {
            NSDictionary *userInfo = @{
                                       NSLocalizedDescriptionKey: NSLocalizedString(@"The method failed.", nil),
                                       NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:@"Time interval doesn't exist for the event name: %@", eventName],
                                       NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(@"Check the methods call sequence", nil),
                                       NSDebugDescriptionErrorKey: [NSString stringWithFormat:@"Parameters: %@", mutableParameters]
                                       };
            NSError *error = [[NSError alloc] initWithDomain:errorDomain code:-1001 userInfo:userInfo];
            [GLLogger.defaultLogger.errorReporter recordError:error];
            block(nil, nil, error);
        }
    });
}

- (NSTimeInterval)getDurationForEventName:(NSString *)eventName {
    NSTimeInterval now = NSDate.timeIntervalSinceReferenceDate;
    
    NSNumber *startNumber = _timestamps[eventName];
    [_timestamps removeObjectForKey:eventName];
    if (startNumber) {
        double startDouble = startNumber.doubleValue;
        NSTimeInterval start = startDouble;
        NSTimeInterval duraton = now - start;
        return duraton;
    } else {
        return 0.0;
    }
}

@end
