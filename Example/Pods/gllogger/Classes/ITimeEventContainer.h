//
//  ITimeEventContainer.h
//  GLLogger
//
//  Created by Kiryl Belasheuski on 3/1/18.
//  Copyright © 2018 Gismart Ltd. All rights reserved.
//

@protocol ITimeEventContainer <NSObject>

- (void)storeStartTimeForEventName:(NSString *)eventName;
- (void)getUpdatedParametersForEventName:(NSString *)eventName
                 withParameters:(NSDictionary *)parameters
                   withNewEvent:(void (^)(NSString *updatedName, NSDictionary* updatedParameters, NSError *error))block;

@end
