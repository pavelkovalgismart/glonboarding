//
//  MultiAnalyticsAdapter.m
//  Pianino
//
//  Created by Alexandra on 7/26/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import "MultiAnalyticsAdapter.h"

@interface MultiAnalyticsAdapter ()

@property(nonatomic, strong) NSArray<id<ILogger>> *adapters;

@end

@implementation MultiAnalyticsAdapter

+ (instancetype)adapterWithAdapters:(NSArray<id<ILogger>> *)adapters {
    MultiAnalyticsAdapter *adapter = [MultiAnalyticsAdapter new];
    adapter.adapters = adapters;
    return adapter;
}

- (void)logEvent:(NSString *)eventName {
    for (id<ILogger> adapter in self.adapters) {
        [adapter logEvent:eventName];
    }
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    for (id<ILogger> adapter in self.adapters) {
        [adapter logEvent:eventName timed:timed];
    }
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    for (id<ILogger> adapter in self.adapters) {
        [adapter logEvent:eventName withParameters:parameters];
    }
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    for (id<ILogger> adapter in self.adapters) {
        [adapter logEvent:eventName withParameters:parameters timed:timed];
    }
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    for (id<ILogger> adapter in self.adapters) {
        [adapter endTimedEvent:eventName withParameters:parameters];
    }
}

@end
