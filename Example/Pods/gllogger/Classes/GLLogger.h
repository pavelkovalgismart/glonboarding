//
//  GLLogger.h
//  DrumPads
//
//  Created by Dzmitry Burchyk on 11/14/16.
//  Copyright © 2016 user. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILogger.h"
#import "IErrorReporter.h"

#define GLLog [GLLogger defaultLogger]

@interface GLLogger : NSObject<ILogger>

@property(nonatomic, strong) id<ILogger> logger;
@property(nonatomic, assign) NSInteger session;
@property(nonatomic, strong) id<IErrorReporter> errorReporter;

+ (instancetype)defaultLogger;

@end
