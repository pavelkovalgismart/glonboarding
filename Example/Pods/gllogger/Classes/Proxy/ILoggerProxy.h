//
//  Proxy.h
//  Real Guitar
//
//  Created by Alex Parkhimovich on 8/30/16.
//  Copyright © 2016 Alex Parhimovich. All rights reserved.
//

#import "ILogger.h"

@protocol ILoggerProxy <ILogger>

- (instancetype)initWithLogger:(id<ILogger>)logger;

@end