//
//  GLLogger.m
//  DrumPads
//
//  Created by Dzmitry Burchyk on 11/14/16.
//  Copyright © 2016 user. All rights reserved.
//

#import "GLLogger.h"

@implementation GLLogger

+ (instancetype)defaultLogger {
    static GLLogger *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[GLLogger alloc] init];
    });
    
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _session = -1;
    }
    return self;
}

- (void)logEvent:(NSString *)eventName {
    NSDictionary *configuredParams = [self configureParams:nil];
    if (configuredParams) {
        [self.logger logEvent:eventName withParameters:configuredParams];
    } else {
        [self.logger logEvent:eventName];
    }
}

- (void)logEvent:(NSString *)eventName timed:(BOOL)timed {
    NSDictionary *configuredParams = [self configureParams:nil];
    if (configuredParams) {
        [self.logger logEvent:eventName withParameters:configuredParams timed:timed];
    } else {
        [self.logger logEvent:eventName timed:timed];
    }
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [self.logger logEvent:eventName withParameters:[self configureParams:parameters]];
}

- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed {
    [self.logger logEvent:eventName withParameters:[self configureParams:parameters] timed:timed];
}

- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters {
    [self.logger endTimedEvent:eventName withParameters:[self configureParams:parameters]];
}

- (NSDictionary *)configureParams:(NSDictionary *)parameters {
    NSAssert(_session > 0, @"Invalid session number");
    if (_session > 0) {
        parameters = [(parameters ? parameters : @{}) mutableCopy];
        ((NSMutableDictionary *)parameters)[@"session"] = @(_session);
    }

    NSAssert(parameters[@"session"], @"Session number required");
    return parameters;
}

@end
