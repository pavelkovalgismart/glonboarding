//
//  MultiAnalyticsAdapter.h
//  Pianino
//
//  Created by Alexandra on 7/26/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import "ILogger.h"

@interface MultiAnalyticsAdapter : NSObject <ILogger>

+ (instancetype)adapterWithAdapters:(NSArray<id<ILogger>> *)adapters;

@end
