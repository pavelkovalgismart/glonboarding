//
//  ICrashReporter.h
//  GLLogger
//
//  Created by Kiryl Belasheuski on 3/3/18.
//  Copyright © 2018 Gismart Ltd. All rights reserved.
//

@protocol IErrorReporter <NSObject>

- (void)recordError:(NSError *)error;

@end

