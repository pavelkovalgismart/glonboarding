//
//  ILogger.h
//  Metronome
//
//  Created by Dzmitry Burchyk on 3/22/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ILogger

- (void)logEvent:(NSString *)eventName;
- (void)logEvent:(NSString *)eventName timed:(BOOL)timed;
- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
- (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed;
- (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;

@end
