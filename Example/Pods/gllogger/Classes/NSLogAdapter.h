//
//  NSLogAdapter.h
//  Metronome
//
//  Created by Dzmitry Burchyk on 3/23/16.
//  Copyright © 2016 Gismart. All rights reserved.
//

#ifdef GISMARTLIB
#import "ILogger.h"
#import "ITimeEventContainer.h"
#else
#import <gllogger/ITimeEventContainer.h>
#import <gllogger/ILogger.h>
#endif

@interface NSLogAdapter : NSObject <ILogger>

- (instancetype)initWithTimeEventContainer:(id<ITimeEventContainer>)container;

@end
