//
//  TimeLoggerContainer.h
//  Pianino
//
//  Created by Kiryl Belasheuski on 2/28/18.
//  Copyright © 2018 Gismart. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifdef GISMARTLIB
#import "ITimeEventContainer.h"
#else
#import <gllogger/ITimeEventContainer.h>
#endif


static NSString *const kDurationTimeKey = @"duration_time";

@interface DurationTimeEventContainer : NSObject <ITimeEventContainer>

@end
