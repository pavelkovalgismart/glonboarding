#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "GLLogger.h"
#import "ILogger.h"
#import "ILoggerProxy.h"
#import "MultiAnalyticsAdapter.h"
#import "NSLogAdapter.h"
#import "DurationTimeEventContainer.h"
#import "ITimeEventContainer.h"
#import "IErrorReporter.h"

FOUNDATION_EXPORT double GLLoggerVersionNumber;
FOUNDATION_EXPORT const unsigned char GLLoggerVersionString[];

