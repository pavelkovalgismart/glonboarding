# glboardinglogger

[![CI Status](https://img.shields.io/travis/Dmitry Burchik/glboardinglogger.svg?style=flat)](https://travis-ci.org/Dmitry Burchik/glboardinglogger)
[![Version](https://img.shields.io/cocoapods/v/glboardinglogger.svg?style=flat)](https://cocoapods.org/pods/glboardinglogger)
[![License](https://img.shields.io/cocoapods/l/glboardinglogger.svg?style=flat)](https://cocoapods.org/pods/glboardinglogger)
[![Platform](https://img.shields.io/cocoapods/p/glboardinglogger.svg?style=flat)](https://cocoapods.org/pods/glboardinglogger)

## Description

Данная библиотека нужна для поддержания общего механизма логирования момента прохождения онборднига. 
Это нужно для более удобного анализа продуктовых метрик - чтобы можно было выделить пользователей, прошедших онбординг. 
Данный ивент **boarding_pass** будет отправлен единожды для пользователя.

Для логирования данного действия необходимо вызвать метод в момент, когда пользователь увидел главный экран приложения

**func logBoardingPass(_ reason: BoardingPassReason)**

Параметром передается соответсвующий reason прохождения онбординга:

1. trial - пользователь прошел в апп купив подписку на онбординге
2. restore - пользователь прошел в апп, успешно совершив восстановление подписки на онбординге (если, конечно, онбординг имеет соответствующую кнопку)
3. free - пользователь прошел в апп, не совершив подписку. Это возможно с помощью нескольих сценариев: 
    * закрыв онбординг с помощью кнопки 'Continue with limited version',
	* онбординг пользователю не был показан по какой-то причине - например, он выключен в конфиге, либо пользователь совершает вторую сессию, 
	  а онбординг настроен т.о., что показывается только на первую сессию (но на первой сессии пользователь ушел из приложения, увидев онбординг)
	
Для free будет дополнительно отправлен ивент **boarding_pass_free**, для restore - **boarding_pass_restore**.
В приложении не нужно создавать проверки на то, был ли ранее вызван данный метод. Данная логика инкапсулирована внутри класса BoardingPassLogger и для повторных вызовов ивент не будет отправлен.

## Author

Dmitry Burchik, dmitry.burchik@gismart.com
