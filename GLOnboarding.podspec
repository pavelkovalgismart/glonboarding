#
# Be sure to run `pod lib lint GLOnboarding.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GLOnboarding'
  s.version          = '0.1.0'
  s.summary          = 'GLOnboarding'
  s.module_name      = 'GLOnboarding'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Onboarding from Guitar Play app'

  s.homepage         = 'https://bitbucket.org/pavelkovalgismart/glonboarding/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => "Custom (Gismart)", :file => "license" }
  s.author           = { 'Pavel Koval' => 'pavel.koval@gismart.com' }
  s.source           = { :git => 'https://bitbucket.org/pavelkovalgismart/glonboarding.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.static_framework = true
  s.swift_version = '4.1'
  s.ios.deployment_target = '8.0'

  s.source_files = 'GLOnboarding/Classes/**/*'
  
  # s.resource_bundles = {
  #   'GLOnboarding' => ['GLOnboarding/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
